var Yelp = function(args) {
  this.params = args.params
  this._create();
}

Yelp.prototype = {
  _create : function() {
    scriptLoader("http://api.yelp.com/phone_search?callback=Yelp.prototype.yelpHandler&cc=US&phone=" + this.params.phone + "&ywsid=" + "-wtvhMFYxA0I3wdRiomuSg")
  },
  yelpNeighborhoodRequest: function(){
    if(this.params.lat && this.params.lng)
    scriptLoader("http://api.yelp.com/neighborhood_search?callback=Yelp.prototype.yelpNeighborhoodHandler&lat="+ this.params.lat +"&long="+ this.params.lng +"&ywsid=" + this.params.ywsid);
  },
  yelpNeighborhoodHandler: function(data){
    var item = $("#header_text h4 span#neighborhood");
    if(data.neighborhoods && data.neighborhoods[0] ){
      var neighborhoodName = data.neighborhoods[0].name.split("|")[0];
      item.html(neighborhoodName + ", " + data.neighborhoods[0].city + ", " + data.neighborhoods[0].state);
    }
    item.parent().slideDown();
  },
  yelpHandler : function(data) {
    best_index = 0;
    best_rating_index = 0;
    var wrapper = $(".outter-container .yelp_rating_widget");
    var pending = wrapper.data("pending");
    if (data.businesses.length > 1) {
      var matched = false;
      if (wrapper.data("yelp_id")){
        for(var i =0; i < data.businesses.length; i++){
          if (wrapper.data("yelp_id") == data.businesses[i].url.split("biz/")[1]) {
            best_rating_index = i;
            matched = true;
            break;
          }
        }
      }
      // if(!matched) {
      //   best_dist = 999;
      //   best_avg_rating = 0;
      //   var addr1 = wrapper.data("addr1");
      //   for (var i = 0; i < data.businesses.length; i++){
      //     var c, d, e, f, g = 0;
      //     tmp_dist = levenshtein_distance(addr1, data.businesses[i].address1, c, d, e, f, g);
      //     if (tmp_dist < best_dist)
      //       best_dist = tmp_dist; best_index = i;

      //     if (parseInt(data.businesses[i].avg_rating) >= best_avg_rating
      //       || parseInt(data.businesses[i].avg_rating) / (parseInt(data.businesses[best_rating_index].avg_rating) + parseInt(data.businesses[i].avg_rating)) > 0.6) {
      //       best_avg_rating = parseInt(data.businesses[i].avg_rating); best_rating_index = i;
      //     }
      //   }
      // }
    }
    var biz = data.businesses[best_rating_index];

    if(biz) {
      if(biz.review_count > 0) {
        var ratingStr = '<a  target="_blank" href="'+ biz.url +'" target="_blank"><span class="diblock"><img src="'+ biz.rating_img_url +'" alt="5-0"></span><div class="count diblock"><span class="text-muted">['+ biz.review_count +']</span></div><span class="diblock"><img alt="yelp_rating" id="yelp_rating_image" src="/images/yelp_icon@2x.png"></span></a>'
        $(".yelp_rating_widget").html(ratingStr);
        reviewStr = "<h3 class='medium_heading strong margin-top-0 margin-bottom-10'>Yelp Reviews</h3><ul class = 'list-unstyled'>"
        var yelp_review_container = $(".reviews-container .yelp_reviews");
        for (var i = 0; i < biz.reviews.length; i++) {
          var review = biz.reviews[i];
          reviewStr = reviewStr +  '<li class="review_line"><div class="row clearfix"><div class="col-xs-12 col-sm-9"><div class="review_image pull-left"><a target="_blank" href=""><img src="'+ review.user_photo_url +'" class="img-responsive img-circle img-thumbnail"><a/></div><div class="review-text"><p><span>'+ review.user_name +'</span><span class="pull-right"><a target="_blank" href="'+ review.url +'"> <img src="'+ review.rating_img_url +'"> </a></span></p><p>'+ review.text_excerpt +'</p></div></div></div></li>'
        }
        reviewStr = reviewStr + "</ul>"
        yelp_review_container.html(reviewStr)
      }
      if($('.yelp_rating_widget').data("update")){
        $.post("/thin/" + $(".yelp_rating_widget").data("thin_id") + "/update_reviews", {rating: biz.avg_rating , review_count: biz.review_count, review_type: "yelp", closed: biz.is_close, neighborhoods: $.map(biz.neighborhoods, function(val, i){ return val.name}).join(","), reviews: biz.reviews.slice(0, 3), yelp_rating_image: biz.rating_img_url, yelp_id: biz.url.split("biz/")[1]})
      }
      

    }
    
  }
};

$(window).load(function(){
  if(window.location.pathname.match(/\/place\/|\/tbusiness\//)){
    var wrapper = $(".yelp_rating_widget");
    if(!wrapper.hasClass("no_api")){
      var yelpObj = new Yelp({
        params : {
          phone     : wrapper.data('phone'),
          ywsid     : wrapper.data("ywsid"),
          addr1     : wrapper.data("addr1"),
          lat       : wrapper.data("lat"),
          lng       : wrapper.data("lng"),
          city      : wrapper.data("city"),
          state     : wrapper.data("state"),
          thin_id   : wrapper.data("thin_id")
        }
      });
    }
  }
});
var CitySearch = function(args){
    this.params = args.params
    this._create();
}
  
  
CitySearch.prototype = {
  _create: function () {
    scriptLoader("http://api.citygridmedia.com/content/places/v2/detail?callback=CitySearch.prototype.cityHandler&format=json&client_ip=" + this.params.client_ip +"&phone=" + this.params.phone.replace(/[^A-Z0-9]+/ig, "") + "&publisher=" + this.params.publisher);
  },
  reviewsApiCall: function(loc, pub_id){
    scriptLoader("http://api.citygridmedia.com/content/reviews/v2/search/latlon?callback=CitySearch.prototype.cityReviewAPiHandler&format=json&radius=1" +"&lat=" + loc.address.latitude + "&lon=" + loc.address.longitude + "&publisher=" + pub_id + "&what=" + loc.categories[0].parent );
  },  
  cityReviewAPiHandler: function(data){
  },  
  cityHandler: function (data) {
    var that = this;
    var ratingStr = "<ul class = 'list-unstyled'>"
    var wrapper =  $(".city_rating_widget");
    if(data.locations && data.locations[0]) {
      var loc = data.locations[0]
      if(loc.review_info.total_user_reviews > 0){
        ratingStr = '<div class = "my-small-rating"><a target="_blank" href="'+ loc.urls.profile_url +'" ><div class="diblock citysearch stars_'+ loc.review_info.overall_review_rating +'"></div><div class="count diblock"><span class="text-muted">['+ loc.review_info.total_user_reviews +']</span></div><span class="diblock"><img alt="city_rating" id="city_rating_image" src="/images/citysearch.jpg"></a></div>'

        wrapper.html(ratingStr);
        ratingStr = "<h3 class='medium_heading strong margin-top-0 margin-bottom-10'>Other Reviews</h3><ul class='list-unstyled'>"
        var city_review_container = $(".reviews-container .city_reviews");
        if(loc.review_info.total_user_reviews_shown > 0){
          for(var i =0; i < loc.review_info.reviews.length; i++){
            ratingStr = ratingStr + '<li class="review_line"><div class="row clearfix"><div class="col-xs-12 col-sm-9"><div class="review-text"><p><span>' + loc.review_info.reviews[i].review_author + '</span><span style="margin-left: 10px;"><a target="_blank" href="'+ loc.review_info.reviews[i].review_url +'"><span class="citysearch stars_'+ loc.review_info.reviews[i].review_rating +'"></span></a></span><span class="pull-right"><a target="_blank" href="'+ loc.review_info.reviews[i].review_url +'"><img src="'+ loc.review_info.reviews[i].attribution_logo +'"></a></span></p><p>'+ loc.review_info.reviews[i].review_text  +'</p></div></div></div></li>'
          }
          ratingStr = ratingStr + "</ul>"

          city_review_container.html(ratingStr)
          
        } 
          
      }

    }
    if(loc && $('.city_rating_widget').data("update")){
      $.post("/thin/" + $(".city_rating_widget").data("thin_id") + "/update_reviews", {rating: loc.review_info.overall_review_rating , review_count: loc.review_info.total_user_reviews_shown, review_type: "city", neighborhoods: loc.neighborhoods.join(", "), reviews: loc.review_info.reviews, website: loc.urls.website_ur, parking: loc.parking, hours: loc.business_hours, payments: $.map(loc.attributes, function(val, i){ var name = ((val.name == "Payments Accepted") ?   val.value : null); return name }).join(", "), closed: ((loc.business_operation_status == "close" || loc.business_operation_status == "closed") ? true : false)})
    }   
  }
}
$(window).load(function(){
  if(window.location.pathname.match(/\/place\/|\/tbusiness\//)){
    var wrapper = $(".city_rating_widget");
    if(!wrapper.hasClass("no_api")){
      new CitySearch({
        params : {
          phone     : wrapper.data('phone'),
          publisher : wrapper.data("publisher"),
          client_ip : wrapper.data("client_ip"),
          pending   : wrapper.data("pending")
        }
      });
    }

  }
});



// var GooglePlaces = function(args) {
//   this.params = args.params
//   this._create();
//   GooglePlaces.prototype.merchantName = this.params.name
// }

// GooglePlaces.prototype = {
//   _create : function() {
//     if(this.params.map){
//       var map = this.params.map;
//       var pyrmont = this.params.latLng;
//     }else{
//     var map;
//         var infowindow;
//         var pyrmont = new google.maps.LatLng(this.params.lat, this.params.lng);
//         map = new google.maps.Map(document.getElementById('map'), {
//           mapTypeId: google.maps.MapTypeId.ROADMAP,
//           center: pyrmont,
//           zoom: 15,
//           panControl: false,
//           zoomControl: true,
//           mapTypeControl: false,
//           scaleControl: false,
//           streetViewControl: false,
//           overviewMapControl: false
//         });
//     }
//         var request = {
//           location: pyrmont,
//           radius: 500,
//           name: this.params.name
//         };
//         infowindow = new google.maps.InfoWindow();
//         var service = new google.maps.places.PlacesService(map);
//         service.search(request, GooglePlaces.prototype.placesHandler);
//         GooglePlaces.prototype.placeMap = map
//   },
//   placesHandler : function(data) {
//     if (data.length > 0) {
//       var reference;
//       for (var i = 0; i < data.length; i++) {
//         if (data[i].name.indexOf(GooglePlaces.prototype.merchantName || null) != -1) {
//           reference = data[i].reference;
//           break;
//         }
//       }
//       // if (!reference) {
//       //  for (var i = 0; i < data.length; i++) {
//       //    if (data[i].vicinity.indexOf(GooglePlaces.prototype.merchantAddressNumber || null) != -1) {
//       //      reference = data[i].reference;
//       //      break;
//       //    }
//       //  }
//       // }
//       if (reference) {
//         var request = {
//             reference: reference
//         };
        
//         service = new google.maps.places.PlacesService(GooglePlaces.prototype.placeMap);
//         service.getDetails(request, GooglePlaces.prototype.detailHandler);  
//       }
//     }
//   },
//   detailHandler : function(place, status) {
//     if (status == "OK") {
//       if (place.rating)
//       {
//         var ratingStr = "<div style='clear:both;'></div><div class='rating'><a target="_blank" href='" +  place.url + "' style='text-decoration:none;cursor:pointer' target='_blank'><div class='rating big_stars'><img src='/assets/star_rating_sprites.gif' class='stars_" + Math.floor(place.rating * 2) +   
//             "' /></div><div id='logo_googlesearch'></div></a></div>";
//         $(".google_rating_widget").append(ratingStr)  
//       }
//       if(place.reviews && place.reviews.length > 0){
//         var rlength = ((place.reviews.length > 3) ? 3 : place.reviews.length);
//         var reviewStr = "<div class='divider'></div><div class='module_left'><img alt='' class='h_icon' src='/assets/google_logo.png'><h3 class='mod_h h_with_icon'>Recent Google reviews</h3>";
//         for(var i =0; i < rlength; i++){
//           reviewStr = reviewStr + "<div class='review google-review " + (i == ((rlength - 1) ? "last" : "")) +  "'><div class='review_text' style='margin-left:0;'><p class='t_small'>" + place.reviews[i].text + "</p><div class='clear'></div><div class='rating big_stars'><img src='/assets/star_rating_sprites.gif' class='stars_" + Math.floor(place.reviews[i].rating * 2)  +   "' /></div><p class='t_mini footer_note'>Google reviewer: " +
//               place.reviews[i].author_name + ", &nbsp;&nbsp;"  +
//               "  <a target='_blank' href='" + place.url + "'> read more</a></p><div class='clear'></div></div></div>";
//         }
//         $(".google_review_widget").append(reviewStr)
//         $(".google_review_widget").slideDown();
//       }
//     }
//   }
// }