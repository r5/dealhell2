require "bundler/capistrano"
require "rvm/capistrano"
# require "whenever/capistrano"

server "192.241.239.133", :web, :app, :db, primary: true
set :rails_env, "production"

set :application, "trenato"
set :user, "app"
set :port, 22
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository, "git@bitbucket.org:r5/dealhell2.git"
set :branch, "master"


default_run_options[:pty] = true
ssh_options[:forward_agent] = true

after "deploy", "deploy:cleanup" # keep only the last 5 releases
after "deploy:update_code", "deploy:copy_old_sitemap"

namespace :deploy do
  %w[start stop restart].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "cd #{current_path} && /etc/init.d/puma_#{application} #{command}"
    end
  end

  desc "Copy last generated sitemap from last release"
  task :copy_old_sitemap do
    run "if [ -e #{previous_release}/public/sitemap.xml.gz ]; then cp -r #{previous_release}/public/sitemap* #{current_release}/public/; fi"
  end

  desc "Deploy with migrations but dont disable the website"
  task :migrations do
    transaction do
      update_code
      symlink
      migrate
    end

    restart
    cleanup
  end

  task :setup_config, roles: :app do
    # sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"
    # sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
    sudo "ln -nfs #{current_path}/config/puma.sh /etc/init.d/puma_#{application}"
    run "mkdir -p #{shared_path}/config"
    run "touch #{shared_path}/config/active_thin_write.txt" # We only write to one table and read from other
    run "touch #{shared_path}/config/active_thin_read.txt"
    run "touch #{shared_path}/config/active_deal_write.txt"
    run "touch #{shared_path}/config/active_deal_read.txt"
    run "touch #{shared_path}/config/8_coupons_previous_page"
    run "touch #{shared_path}/config/mytime_previous_page"
    run "echo 'mdeals' > #{shared_path}/config/active_deal_write.txt"
    run "echo 'deals' > #{shared_path}/config/active_deal_read.txt"
    run "echo 'mthin_merchants' > #{shared_path}/config/active_thin_write.txt"
    run "echo 'thin_merchants' > #{shared_path}/config/active_thin_read.txt"
    put File.read("config/database.example.yml"), "#{shared_path}/config/database.yml"
    puts "Now edit the config files in #{shared_path}."
  end
  after "deploy:setup", "deploy:setup_config"

  task :symlink_config, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/GeoLite2-City.mmdb #{release_path}/GeoLite2-City.mmdb"
  end
  after "deploy:finalize_update", "deploy:symlink_config"

  desc "Make sure local git is in sync with remote."
  task :check_revision, roles: :web do
    unless `git rev-parse HEAD` == `git rev-parse origin/master`
      puts "WARNING: HEAD is not the same as origin/master"
      puts "Run `git push` to sync changes."
      exit
    end
  end
  before "deploy", "deploy:check_revision"
end