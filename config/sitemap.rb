# Set the host name for URL creation
SitemapGenerator::Sitemap.create_index = true
SitemapGenerator::Sitemap.default_host = "#{WEBSITE_NAME}"
SitemapGenerator::Sitemap.create do
  
  group(:sitemaps_path => "sitemap/", :filename => :static) do
    add "/privacy", :lastmod => 30.days.ago, :changefreq => 'monthly', :priority => 0.5
    add "/terms", :lastmod => 30.days.ago, :changefreq => 'monthly', :priority => 0.5
    add "/sitemap", :lastmod => Time.zone.now, :changefreq => 'daily', :priority => 1.0
    add "/", :lastmod => 30.days.ago, :changefreq => 'monthly', :priority => 1.0
  end

  group(:sitemaps_path => "sitemap/", :filename => :thins) do
    services = Service.enabled.all
    start_date = DateTime.parse("Sun, 29 Dec 2013")
    end_date = DateTime.now
    multi = [*3000..3030].sample
    limit = (200000 + ((end_date.hour - start_date.hour) * multi))
    i = 1
    ThinMerchant.select("city, state, name, id, main_service_id").where(:discardable => false).find_each  do |thin|
      service = services.find{|s| s.ass_id == thin.main_service_id}
      next unless service
      service_name = service.name
      i = i + 1
      break if i > limit
      add thin.slug_path(service_name), :lastmod => 2.days.ago, :changefreq => 'monthly', :priority => 0.5
    end
  end

  # group(:sitemaps_path => "sitemap/", :filename => :thin_browser) do
  #   services = Service.all
  #   ThinMerchant.select("distinct(zip)").where(:discardable => false).map(&:zip).uniq.compact.each do |zip|
  #     next unless zip.to_s.match(/^\d\d\d\d\d$/)
  #     add zip_deals_path(:zip => zip), :lastmod => 2.days.ago, :changefreq => 'monthly', :priority => 0.8
  #   end
  # end

  # group(:sitemaps_path => "sitemap/", :filename => :deals) do
  #   services = Service.all
  #   Deal.select("city, state, name, id, service_id").where("dealsource = 'MyTime' || expirationdate < ?", Date.today).find_each do |deal|
  #     service =  services.find{|s| s.id == deal.service_id}
  #     next unless service
  #     next unless service.status != "enabled"
  #     service_name = service.name
  #     next unless service_name
  #     add deal.slug_path(service_name), :lastmod => 30.days.ago, :changefreq => 'daily', :priority => 0.6
  #   end
  # end

  # group(:sitemaps_path => "sitemap/", :filename => :browse_deals) do
  #   Deal.select("distinct city, state").where("dealsource = 'MyTime' || expirationdate < ?", Date.today).where("dealsource = 'MyTime' || expirationdate < ?", Date.today).find_each do |deal|
  #     add deals_path(:city => deal.city, :state => deal.state), :lastmod => 30.days.ago, :changefreq => 'daily', :priority => 0.9
  #   end
  # end

  # group(:sitemaps_path => "sitemap/", :filename => :browse_deal_services) do
  #   services = Service.all
  #   Deal.select("distinct city, state, service_id").where("dealsource = 'MyTime' || expirationdate < ?", Date.today).where("dealsource = 'MyTime' || expirationdate < ?", Date.today).find_each do |deal|
  #     service =  services.find{|s| s.id == deal.service_id}
  #     next unless service
  #     next unless service.status != "enabled"
  #     service_name = service.name
  #     add service_deals_path(:state => deal.state, :city => deal.city, :service => service_name), :lastmod => 30.days.ago, :changefreq => 'daily', :priority => 0.7
  #   end
  # end
end
