require "subdomain"
Trenato::Application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  get "my_pets", :to => 'users#pets', :as => "my_pets"
  resources :pets
  resources :comments
  constraints(Subdomain) do
    resources :bookmarks
    resources :recommendations
    match "/" => 'splash#welcome', :via => "get" 
    get "/myframe", :to => "splash#myframe", :as => :myframe
    get '/place/:state/:city/:service/:business_name/:id', :to => "thin#show", :as => :show_thin_city, :constraints => { :business_name => /.*/}
    get "/place/:service/:business_name/:id", :to => "thin#show", :as => :show_thin, :constraints => { :business_name => /.*/ }
    get '/search(/:page)', :to => "deals#index", :as => :search_deals, constraints: {page: /[0-9]+/}
    get '/:state/:city(/:page)', :to => "deals#index", :as => :deals, constraints: {page: /[0-9]+/}
    get '/:state/:city/:service(/:page)', :to => "deals#index", :as => :service_deals, constraints: {page: /[0-9]+/}
    get "/:state/:city/:zip(/:page)", :to => "deals#index", :as => :zip_deals, constraints: {zip: /\d\d\d\d\d/, page: /[0-9]+/}
    get "/:state/:city/:zip/:service(/:page)", :to => "deals#index", :as => :zip_service_deals, constraints: {zip: /\d\d\d\d\d/, page: /[0-9]+/}
    resources :thin do
      member do
        post :update_reviews
      end
    end
    resources :location_autocomplete, :only => [:index]
  end

  
  
  get "/about", :to => "splash#about_us"
  get "/privacy", :to => "splash#privacy"
  get "/terms", :to => "splash#terms"
  root 'pets#index'
  
  
  # get '/deals', :to => "deals#index", :as => :deals
  # post 'get_zip', :to => "deals#get_zip", :as => :get_zip

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
