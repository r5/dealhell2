workers 2
threads 0, 16
environment 'production'
daemonize true
pidfile '/tmp/puma.pid'
bind 'unix:///tmp/puma.sock'
