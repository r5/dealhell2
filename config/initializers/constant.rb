# SITENAME= "Trenato"
# WEBSITE_NAME = "http://www.trenato.com"
EIGHT_COUPONS = "b568cb842bfb801a8c62a08596279ea014b41ec7118f53e54988b6c54977d8091b5ac06f850ad5bc94fe3fb4a729573f"

URL_TYPES = {"local" => 1, "shop" => 2, "play" => 3, "eat" => 4}

YELP = "-wtvhMFYxA0I3wdRiomuSg"
CITY_PUBLISHER = "10000000075"

YELP_RATING_TO_IMAGE = {
  0.0 => "",
  1.0 => "http://s3-media3.ak.yelpcdn.com/assets/2/www/img/f64056afac01/ico/stars/stars_1.png",
  1.5 => "http://s3-media1.ak.yelpcdn.com/assets/2/www/img/11e62fee886f/ico/stars/stars_1_half.png",
  2.0 => "http://s3-media4.ak.yelpcdn.com/assets/2/www/img/b561c24f8341/ico/stars/stars_2.png",
  2.5 => "http://s3-media2.ak.yelpcdn.com/assets/2/www/img/c7fb9aff59f9/ico/stars/stars_2_half.png",
  3.0 => "http://s3-media1.ak.yelpcdn.com/assets/2/www/img/34bc8086841c/ico/stars/stars_3.png",
  3.5 => "http://s3-media3.ak.yelpcdn.com/assets/2/www/img/5ef3eb3cb162/ico/stars/stars_3_half.png",
  4.0 => "http://s3-media2.ak.yelpcdn.com/assets/2/www/img/c2f3dd9799a5/ico/stars/stars_4.png",
  4.5 => "http://s3-media4.ak.yelpcdn.com/assets/2/www/img/99493c12711e/ico/stars/stars_4_half.png",
  5.0 => "http://s3-media3.ak.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/stars_5.png",
}