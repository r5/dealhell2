module ActiveRecordOverride
  def self.included(base)
    base.extend(self)
  end

  def my_import(records=[])
    columns = self.connection.schema_cache.columns_hash(table_name)
    key_list = columns.keys
    values = records.map do |record|
      formatted = key_list.map do |cname|
        if columns[cname].sql_type.to_s == "point"
          record[cname] = "Point(#{record.location["lat"]}\, #{record.location["lon"]})"
        elsif cname == "created_at" || cname == "updated_at"
          self.connection.quote(Time.zone.now, columns[cname])
        else
          self.connection.quote(record[cname], columns[cname])
        end
      end.join(", ")
      "( " + formatted + " )"
    end.join(", ")
    ActiveRecord::Base.connection.execute("INSERT INTO #{table_name} (#{key_list.join(', ')}) VALUES #{values}")   
  end
end