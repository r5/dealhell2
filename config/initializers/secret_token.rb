# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Trenato::Application.config.secret_key_base = 'e67ab85a28aca27efa376643e234088fe9e5dfcb240d5454b5a3a6e0c5ba635007c2162bed9c233554ea72909fc14de5e5c523687c6df90f2f50aa6d19173938'
