namespace :eight_coupons do
  
  desc "get all categories"
  desc "get_deals"
  task :get_deals => :environment do
    DealWriter.where("dealsource != 'MyTime'").delete_all
    DealWriter.get_8_coupons_today
    # DealWriter.get_mytime_deals
    DealWriter.switch_deal_reader_table
    system("/etc/init.d/puma_trenato restart")
    raise ("Yay!!! My Job Ran Fine")
  end

  desc "Make sure the ThinMerchant table has all indexes after the new import"
  task :update_indexes_for_new_thins => :environment do
    # CREATE TABLE recipes_new LIKE production.recipes;
# INSERT recipes_new SELECT * FROM production.recipes; - See more at: http://www.tech-recipes.com/rx/1487/copy-an-existing-mysql-table-to-a-new-table/#sthash.PBIOkBqB.dpuf
    table_name = "thin_merchants"
    dump_path = ENV['path']
    system("mysql -u #{Rails.configuration.database_configuration[Rails.env]["username"]} #{Rails.configuration.database_configuration[Rails.env]["password"] ? ('-p' + Rails.configuration.database_configuration[Rails.env]["password"]) :  ''} #{Rails.configuration.database_configuration[Rails.env]["database"]} < #{dump_path}")
    ThinMerchant.update_read_from("mthin_merchants")
    system("/etc/init.d/puma_trenato restart")
    #ThinMerchantWriter.where("main_service_id not in (?)" ,[40, 38, 60, 39, 62]).delete_all
    ThinMerchantWriter.where(:discardable => true).delete_all
    ActiveRecord::Migration.execute "alter table #{table_name} engine myisam"
    ActiveRecord::Migration.add_column :thin_merchants,  :lat, :decimal, :precision => 15, :scale => 10
    ActiveRecord::Migration.add_column :thin_merchants,  :lon, :decimal, :precision => 15, :scale => 10
    ActiveRecord::Migration.add_column :thin_merchants, :closed, :boolean
    # Some of the locations have string lat lon and some have float, that is y we replace the \\'
    # TODO a location can also be a symbol 
    # which give the yaml as  "---\n:lat: '33.866272'\n:lon: '-117.941986'\n"
    # the following work with "---\nlat: 37.78236\nlon: -122.402096\n" only
    # ActiveRecord::Migration.execute "update thin_merchants set lon = REPLACE(REPLACE(SUBSTRING_INDEX( REPLACE(location, '\n:', '\n') , '\nlon: ', -1 ), '\n', ''), '\\'', '')"
     
    # ActiveRecord::Migration.execute "update thin_merchants set lat =  REPLACE(REPLACE(REPLACE(SUBSTRING_INDEX( REPLACE(location, '\n:', '\n') , '\nlon: ', 1 ), '\n', ''),'---lat: ', ''), '\\'', '')"
    ActiveRecord::Migration.execute "alter table #{table_name} add latlon point not null"
    ActiveRecord::Migration.execute "update thin_merchants set latlon = GeomFromText(concat(\'Point(\',       REPLACE(REPLACE(REPLACE(SUBSTRING_INDEX( REPLACE(location, '\n:', '\n') , '\nlon: ', 1 ), '\n', ''),'---lat: ', ''), '\\'', '')            ,\' \',     REPLACE(REPLACE(SUBSTRING_INDEX( REPLACE(location, '\n:', '\n') , '\nlon: ', -1 ), '\n', ''), '\\'', '')       ,\')\'))"
    # ActiveRecord::Migration.execute "create trigger set_latlon_thin_point before insert on #{table_name} for each row set new.latlon = GeomFromText(concat('Point(',new.lat,' ',new.lon,')'));" 
    ActiveRecord::Migration.execute "DROP INDEX index_thin_merchants_on_id_hash ON #{table_name}"
    ActiveRecord::Migration.execute "create spatial index thin_lat_lon on #{table_name} (latlon)"
    ActiveRecord::Migration.execute "CREATE FULLTEXT INDEX fulltext_thin_name ON #{table_name} (name)"
    ThinMerchant.update_read_from("thin_merchants")
    system("/etc/init.d/puma_trenato restart")
    raise ("Yay!!! My ThinMerchant Job Ran Fine")
  end
end