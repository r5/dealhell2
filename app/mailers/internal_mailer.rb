class InternalMailer < ActionMailer::Base
  default from: "vipin.itm@gmail.com"

  def exception_email(message)
    mail({:subject => "Exception on #{PRODUCT_NAME}",
          :to      => "eng@#{HOSTNAME}",
          :body    => "#{message}"})
  end
end
