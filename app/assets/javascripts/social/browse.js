function showPageModal($cover){
  $('#cover-image-model .modal-body .modal-cover-container').html($cover.clone())
  $('#cover-image-model').modal('show')
}
$(document).ready(function() {
  $("#browse-page .cover-image a.modal-link").click(function(){
    showPageModal($(this).parents(".cover-image").first())
    return false;
  });

  $(".my-cover-image").hover(function(){ $(this).find(".mask-link").fadeIn("fast");},function(){$(this).find(".mask-link").fadeOut("fast");})
});