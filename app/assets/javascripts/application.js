// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap/dropdown.js
// require twitter/bootstrap/tooltip.js
//= require twitter/bootstrap/alert.js
//= require twitter/bootstrap/modal.js
//= require twitter/bootstrap/collapse.js
//= require jquery.validate.min.js
//= require jquery.cookie.js
//= require jquery.lazyload.js
// require twitter/bootstrap/affix.js
//= require underscore.js
//= require typeahead.bundle.min.js
//= require thin_reviews.js
//= require deals.js

function submenuDropdownHoverFix(){
   $('li.dropdown-submenu .dropdown-toggle-submenu').on('click', function(event) {
        // Avoid following the href location when clicking
        event.preventDefault(); 
        // Avoid having the menu to close when clicking
        event.stopPropagation(); 
        $(".dropdown-menu").find(".open").removeClass("open")
        // If a menu is already open we close it
        $(this).parent().addClass('open');
        // var menu = $(this).parent().find("ul");
        // var menupos = $(menu).offset();

        // if (menupos.left + menu.width() > $(window).width()) {
        //     var newpos = -$(menu).width();
        //     menu.css({ left: newpos });    
        // } else {
        //     var newpos = $(this).parent().width();
        //     menu.css({ left: newpos });
        // }show
    }); 
}



// function dropdownOnHover(){
//   $('.navbar .dropdown').hover(function() {
//   $(this).find('.dropdown-menu').first().show();
//   }, function() {
//     $(this).find('.dropdown-menu').first().hide()
//   });
// }

function tweet_click($that){
  window.open('https://twitter.com/share?url='+encodeURIComponent($that.data("url"))+'&text='+encodeURIComponent($that.data("text")),'sharer','toolbar=0,status=0,width=626,height=436');
  return false;
}

function fbs_click($that) {
  window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent($that.data("url"))+'&t='+encodeURIComponent($that.data("title")),'sharer','toolbar=0,status=0,width=626,height=436');
  return false;
}


function windowOpener(url){
  window.open(url,'sharer','toolbar=0,status=0,width=626,height=436');
  return false;
}


function equalHeight($groups) {
   $groups.each(function(){
      tallest = 0;
      $items = $(this).find(".full-height-col")
      $items.each(function() {
         thisHeight = $(this).height();
         if(thisHeight > tallest) {
            tallest = thisHeight;
         }
     });
     $items.height(tallest);
   })

}
function scriptLoader(url) {
  var script = document.createElement('script');
  script.src = url;
  script.type = 'text/javascript';
  var head = document.getElementsByTagName('head').item(0);
  head.appendChild(script);
}

$(document).ready(function () {
  // dropdownOnHover()
  submenuDropdownHoverFix()
  // $(".my-tooltip").tooltip();

  // LAGECY  No Need for the code following as we use IP
  // if(!$.cookie('lat') && !$.cookie('lng') && !$.cookie('skipped-zip-modal')){
  //   $("#zip_collector").modal({
  //     backdrop: 'static',
  //     keyboard: false,
  //     show: false
  //   });
  //   $("#zip_collector").on('hidden.bs.modal', function(){
  //     $.cookie('skipped-zip-modal', true, { expires: 2, path: '/' })
  //   });
  //   setTimeout(function(){
  //     $("#zip_collector").modal("show")
  //   }, 1000)
  // }

  $("#real-myframe").css({height: $(window).height() -  $("header").offset().top})


  jQuery.validator.addMethod("zipcode", function(value, element) {
    return this.optional(element) || /\d{5}-\d{4}$|^\d{5}$/.test(value);
  }, "The ZIP is invalid");

  // $("#zip_collector form").validate({
  //   errorClass: "t_error",
  //   errorElement: "p"
  //  });


  // var locations = new Bloodhound({
  //   datumTokenizer: function(d) {
  //     return Bloodhound.tokenizers.whitespace(d.val); 
  //   },
  //   queryTokenizer: Bloodhound.tokenizers.whitespace,
  //   limit: 10,
  //   remote: '/location_autocomplete.json?q=%QUERY',
  //   sorter: function (items) {
  //     return _.sortBy(items, function(obj){ return obj.val })
  //   },
  //   // prefetch: {
  //   //   url: "/location_autocomplete_file.json",
  //   //   filter: function(list) {
  //   //     debugger
  //   //     return $.map(list, function(loc) { return { val: loc}; });
  //   //   }
  //   // }
  // });
 
  // locations.initialize();

  var locationSource = function(query, cb){
    $.get("/location_autocomplete.json?q=" + query, function(data){
      var locations = _.map(data, function(line){ return {location: line} });
      cb(locations)
    });
  }

  $('.location_typeahead').typeahead({
    minLength: 3,
    autoselect: true
  }, 
  {
    name: 'location',
    displayKey: 'location',
    source: locationSource
  }).on('typeahead:opened', function($e){
    var owidth = $($e.delegateTarget).outerWidth();
    $(".tt-dropdown-menu").width(owidth)
  });
});
