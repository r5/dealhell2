//= require thin_reviews.js

// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// We need to bind the map with the "init" event otherwise bounds will be null
var latLngList = []

function fitMapBounds(){
  var map = $('#map_canvas').gmap('get', 'map');
  var bounds = new google.maps.LatLngBounds ();
  for ( var i = 0; i < latLngList.length; i++ ) {
    bounds.extend (latLngList[i]);
  }
  google.maps.event.trigger(map, "resize")
  map.fitBounds(bounds);
}

// function displayHideRightsideBlock(){
//   if($("#deals_container .right_side_block").is(":visible")){
//     $("#deals_container .deal_blk").removeClass("fix-max-width")
//   }else{
//     $("#deals_container .deal_blk").addClass("fix-max-width")
//   }
// }

function initializeMap() {
  loadGmapLib();
  if($('#map-canvas')[0]){
    var lat = $('#map-canvas').data("lat");
    var lng = $('#map-canvas').data("lng");
    var myLatlng = new google.maps.LatLng(lat, lng);

    var mapOptions = {
      zoom: 13,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        animation: google.maps.Animation.DROP
    });
  }
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.id = "mapv3-lib"
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 
      'callback=initializeMap';
  document.body.appendChild(script);
}

function loadGmapLib(){
  if(window.location.pathname.match(/\/places\//)){
    // script = document.createElement('script');
    // script.type = 'text/javascript';
    // script.src = '/assets/markerclusterer.js';
    // document.body.appendChild(script);
    $.getScript("/javascripts/jquery.ui.map.min.js", function(){
      
    })

  }
}

if(window.location.pathname.match(/places|place|tbusiness/)){
window.onload = loadScript;
}

function getDealTemplate(marker){
  var $ele = $("#"+ marker.id.replace("marker", "deal"))
  var obj = {image: $ele.find("img.deal_image").data("original"), name: $ele.find("h3 span.bold").text(), address: $ele.find(".address").text(), service: $ele.find(".service").text(), slug_path: $ele.find(".slug").attr("href") }
  return _.template($("#map-deal-index").html(), obj)
}

function initGmap(){
  $('#map_canvas').gmap({'zoom': 16, 'scrollwheel': false}).bind('init', function(evt, map) { 

    //$("#map_canvas_wrap").css( "width", $("#map_canvas_container").width());
    // var bounds = map.getBounds();
    // var southWest = bounds.getSouthWest();
    // var northEast = bounds.getNorthEast();
    // var lngSpan = northEast.lng() - southWest.lng();
    // var latSpan = northEast.lat() - southWest.lat();
    for ( var i = 0; i < marker_locations.length; i++ ) {
      var lat = marker_locations[i].latitude
      var lng = marker_locations[i].longitude
      // var lat = southWest.lat() + latSpan * Math.random();
      // var lng = southWest.lng() + lngSpan * Math.random();
      latLngList[i] = new google.maps.LatLng(lat, lng)
      $('#map_canvas').gmap('addMarker', { 
        'position': latLngList[i],
        'bounds': true,
        'id': "marker" + marker_locations[i].id.toString(),
        'icon': '/assets/MapMarker_Marker_Outside_Azure.png',
      }).click(function() {
        $('#map_canvas').gmap('openInfoWindow', { content : getDealTemplate(this) }, this);
      });
    }
    // $('#map_canvas').gmap('set', 'MarkerClusterer', new MarkerClusterer(map, $(this).gmap('get', 'markers')));
    fitMapBounds();
    $(window).resize(function() {
      fitMapBounds();
    });

   
    // $(".business-index").mouseenter( 
    //   function(){ 
    //     var markerId = $(this).attr("id").replace("deal", "marker");
    //     var myMarker = $('#map_canvas').gmap('get', 'markers')[markerId];
    //     myMarker.setIcon("/assets/MapMarker_Marker_Outside_Azure-big.png");
    //   }
    // ).mouseleave(
    //   function(){ 
    //     var markerId = $(this).attr("id").replace("deal", "marker");
    //     var myMarker = $('#map_canvas').gmap('get', 'markers')[markerId];
    //     myMarker.setIcon("/assets/MapMarker_Marker_Outside_Azure.png"); 
    //   }
    // );
  });
  $("#map_canvas").addClass("mapCreated")
}

$(document).ready(function() {
  $("img.lazy").show();
  $("img.lazy").lazyload();

  if($("#recommendation_text_modal")[0]){
    $("#recommendation_text_modal").modal("show");
  }

  $("#map-pull-down").click(function(){
    var $mapCanvas = $("#map_canvas");
    if(!$mapCanvas.hasClass("mapCreated")){
      initGmap();
    }
    if($mapCanvas.hasClass("expanded")){
      $mapCanvas.animate({ height: 0 }, 500, function(){
         $(window).trigger('resize');
         $mapCanvas.removeClass("expanded");
      });
      $(this).find("span").text("View Map");
    }else{
      $mapCanvas.animate({ height: $(window).height() -  $("#map_canvas_container").offset().top }, 500, function(){
        $mapCanvas.addClass("expanded");
        $(window).trigger('resize');
      });
      $(this).find("span").text("Hide Map");
    }

    return false;

  })

  // $("#map_canvas_container").affix({
  //   offset: {
  //     top: $("#map_canvas_container").offset().top,
  //   }
  // });
  // $("#map_canvas_container").css({width: $("#map_canvas_container").width()})
});