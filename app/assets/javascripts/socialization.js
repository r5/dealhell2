// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require underscore.js
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap/dropdown.js
//= require jquery.validate.min.js
//= require jquery.cookie.js
//= require jquery.mosaicflow.min
//= require twitter/bootstrap/transition.js
//= require twitter/bootstrap/button.js
//= require grid-a-licious.js
//= require social/browse
//= require cloud_grid.js
//= require jquery.nested.js
//= require twitter/bootstrap/collapse.js
//= require twitter/bootstrap/modal.js
//= require twitter/bootstrap/carousel.js
//= require social/profile.js

// function makeAlbumGrids(className, itemName){
//   debugger
//   // Create a random number of items between 100 and 200.
//     var $element;
//     var children = [];
//     var $gridContainer = $(className);
//     var items = Math.floor(100 + Math.random() * 200);
//     var columns;
//     var rows;

//     $gridContainer.find(itemName).each(function(){
//       $element = $(this)
//       $.data($element, 'grid-columns', $element.data("columns"));
//       $.data($element, 'grid-rows',$element.data("rows"));
//        children.push($element);
//     });

//     $gridContainer.cloudGrid({
//       children: children,
//       gridGutter: 1,
//       gridSize: 200
//     });

//     $(window).on('resize', function() {
//       $gridContainer.cloudGrid('reflowContent');
//     })
// }

$(document).ready(function() {
  $("#boxy").gridalicious({width: 350, gutter: 20, selector: '.cover-image'});
  $(".login").on("click", function(){
    $('.modal').modal('hide');
    $("#login_modal").modal("show");
    return false;
  });
  $(".signup").on("click", function(){
    $('.modal').modal('hide');
    $("#signup_modal").modal("show");
    return false;
  });
});
  // $(".album-gallary").nested({
  //   minWidth: 200,
  //   selector: '.figure',
  //   resizeToFit: true,
  //   animate: false,
  //   resizeToFitOptions: { 
  //     resizeAny: true
  //   }
  // });
  // Create a random number of items between 100 and 200.
  // var $element;
  // var children = [];
  // var $gridContainer = $('#boxy');
  // var items = Math.floor(100 + Math.random() * 200);
  // var columns;
  // var rows;

  // $gridContainer.find("figure").each(function(){
  //   $element = $(this)
  //   $.data($element, 'grid-columns', $element.data("columns"));
  //   $.data($element, 'grid-rows',$element.data("rows"));
  //    children.push($element);
  // });

  

  // $gridContainer.cloudGrid({
  //   children: children,
  //   gridGutter: 0,
  //   gridSize: 240,
  //   gridHeight: 188
  // });

  // $(window).on('resize', function() {
  //   $gridContainer.cloudGrid('reflowContent');
  // })
  // $('.album-gallary').mosaicflow({
  //   itemSelector: 'figure',
  //   minItemWidth: 200,
  //   itemHeightCalculation: "figure img"
  // });

 //  $('#boxy').boxify({
 //    width: "240px",
 //    background: "#fff",
 //    bordercolor: "#bbb",
 //    color: "#444",
 //    margin: "0px",
 //    padding: "0px"
 //  });

 // $(window).on("resize", function(){
 //  $('#boxy').boxify({
 //    width: "240px",
 //    background: "#fff",
 //    bordercolor: "#bbb",
 //    color: "#444",
 //    margin: "0px",
 //    padding: "0px"
 //  });
 // });



