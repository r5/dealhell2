class ThinMerchant < ActiveRecord::Base
  # self.rgeo_factory_generator = RGeo::Geos.method(:factory)

  # Every time you bring in more thins merchnats MAKE SURE U REMOVE DISCARDABLEin

  serialize :location
  serialize :service_id
  serialize :category_id
  serialize :categories
  serialize :hours
  serialize :neighborhoods
  serialize :yp_hours
  serialize :brands
  serialize :yelp_reviews
  serialize :city_reviews
  serialize :google_reviews

  has_many :recommendations
  has_many :bookmarkes
  # has_many :likes
  
  acts_as_mappable
  # default_scope do 
  #   unless Rails.env.development? # This is temp
  #     where(:discardable => false, :blacklisted => false)
  #   end 
  # end
  belongs_to :service, :foreign_key => "main_service_id", :primary_key => "ass_id"
  include ActiveRecordOverride

  attr_accessor :lat, :lng, :lon

  if Rails.env.development?
    self.table_name = File.open(Rails.root.join("tmp","active_thin_read.txt"), "r")do |f| 
      name = f.read.chomp
      name.present? ? name : "thin_merchants"
    end
  else
    self.table_name = File.open(Rails.root.join("..", "..", "shared", "config", "active_thin_read.txt"), "r")do |f| 
      name = f.read.chomp
      name.present? ? name : "thin_merchants"
    end
  end

  def review_and_rating 
    # TODO Add other ratings
    if self.yelp_review_count && self.yelp_review_count > 0
      [self.yelp_rating.to_f.to_s.gsub(".", "-"), self.yelp_review_count, "yelp"]
    elsif false
      []
    end
  end

  def lng
    self.lon
  end

  def content
    self.name + " offers " + (self.categories.class == Array ? self.categories.to_sentence : self.categories ) + " in #{self.city}, #{self.state}"
  end

  def all_services
    # TODO we should have an attribute called mytime service and use that. We might have to add more services later. It does not make sense
    Service.where(:id => self.service_id).enabled.map(&:name)
    # self.categories.join(", ")
  end

  def dealtitle(service_name=nil)
    service_name = service_name || self.service.name
    service_name + " from " + self.name
  end

  def slug_path(service_name = nil)
    service_name = service_name || self.service.name
    if !self.state.present? and !self.city.present?
      URI.escape("/place/#{service_name}/#{CGI.escape(self.name)}/#{self.id}")
    else
      URI.escape("/place/#{self.state}/#{self.city}/#{service_name}/#{CGI.escape(self.name)}/#{self.id}")
    end
  end

  def slug_url(service_name=nil)
    WEBSITE_NAME + slug_path(service_name)
  end

  def my_slug_path(service_name = nil, category_name=nil)
    service_name = service_name || self.service.name
    category_name = category_name || Category.find(self.category_id.first).name
    if self.state.present? and self.city.present?
      URI.escape("http://www.mytime.com/deals/#{self.state}/#{self.city}/#{category_name}/#{service_name}/#{self.name.gsub(" ", "-")}/mt-#{self.id_hash}")
    else
      ""
    end
  end

  def full_address
    adr = ""
    adr += street_address + ", <br />" if street_address.present?
    adr += city + ", " if city.present?
    adr += state + " " if state.present?
    adr += zip if zip.present?
    adr.html_safe
  end

  def slug_url
    WEBSITE_NAME + slug_path
  end

  def self.update_read_from(from)
    if Rails.env.development?
      File.open(Rails.root.join("tmp","active_thin_read.txt"), "w")do |f| 
        f.write from
      end
    else
      table_name = File.open(Rails.root.join("..", "..", "shared", "config", "active_thin_read.txt"), "w")do |f| 
        f.write from
      end
    end
  end

# NOTE  SMART BITE

# https://dev.mysql.com/doc/refman/5.5/en/fulltext-boolean.html

# Boolean searching has two deficiencies: 1) results are not sorted by relevance and; 2) no method by which to weigh certain columns. There is a way around both of these problems. For example, if I have a table of articles and want to weigh the title more heavily than the text, I can do the following:

# SELECT *, ( (1.3 * (MATCH(title) AGAINST ('+term +term2' IN BOOLEAN MODE))) + (0.6 * (MATCH(text) AGAINST ('+term +term2' IN BOOLEAN MODE))) ) AS relevance FROM [table_name] WHERE ( MATCH(title,text) AGAINST ('+term +term2' IN BOOLEAN MODE) ) HAVING relevance > 0 ORDER BY relevance DESC;

# Here we artificially manipulate the relevancy score to give title more weight by multiplying by the constant 1.3. In the above query, it doesn't seem to matter whether I have 3 fulltext indexes or just one comprising the title and text columns. From my testing, the results appear to be the same.


def self.near(options={})
  mappable = options[:origin]
  match_ids= options[:match]
  per_page= options[:per_page] || 40
  url_type= URL_TYPES[options[:url_type]]
  page= options[:page] || 1
  service_id= options[:service_id]
  url_type = nil
  match_name = options[:match_name]
  explain = options[:explain]
  distance = options[:distance] || 20
  and_query = ""
  if match_ids.present?
    and_query << " and id in (#{match_ids.join(',')})"
  end
  if service_id
    and_query << " and main_service_id = #{service_id}"
  end
  bound = GeoKit::Bounds.from_point_and_radius(mappable, distance)
  if match_name.present?
    # TODO Conver this to boolean mode. It is so much faster. Take a look here http://www.slideshare.net/billkarwin/practical-full-text-search-with-my-sql
    and_query << " and MATCH(name) AGAINST (#{self.sanitize(match_name)} IN NATURAL LANGUAGE MODE)"
sql = <<SQL
#{explain ? 'explain' : ''} select *, GLength(LineStringFromWKB(LineString((latlon), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as geo_distance from thin_merchants where MBRWithin(latlon, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) #{and_query} limit #{per_page} offset #{(page.to_i - 1) * 40}
SQL
count_sql = <<SQL
#{explain ? 'explain' : ''} select COUNT(*), GLength(LineStringFromWKB(LineString((latlon), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as geo_distance from thin_merchants where MBRWithin(latlon, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) #{and_query}
SQL
  else
    sort_by = "geo_distance asc"
sql = <<SQL
#{explain ? 'explain' : ''} select *, GLength(LineStringFromWKB(LineString((latlon), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as geo_distance from thin_merchants where MBRWithin(latlon, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) #{and_query} order by #{sort_by} limit #{per_page} offset #{(page.to_i - 1) * 40}
SQL
count_sql = <<SQL
#{explain ? 'explain' : ''} select COUNT(*), GLength(LineStringFromWKB(LineString((latlon), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as geo_distance from thin_merchants where MBRWithin(latlon, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) #{and_query}
SQL
  end
 
  thins = find_by_sql(sql)
  total_entries = count_by_sql(count_sql)
  eagerLoadFor(thins, [{:service => :category}])
  [thins, total_entries, total_entries, total_entries/40.0]
end


  def self.import_another_file(filename = "thin_merchants.txt")
    categories = Category.all
    services = Service.all
    File.open("/home/vipin/scrapper-old-data/facebook/thin_merchants_fb.txt").lines.each_slice(1000) do |lines|
      puts "asd"
      thin_merchants = lines.map do |line|
        line = JSON.parse(line.chomp)
        category = categories.find { |cat| cat.id == (line["category_id"] && line["category_id"].first) }
        service = services.find { |s| s.id == line["main_service_id"] }
        next if service.nil?
        line["id_hash"] = line["id"]
        line["lat"] = line["location"]["lat"]
        line["lon"] = line["location"]["lon"]
        line.delete("id")
        ThinMerchant.new(line.except("about", "fbid", "like_count", "checkin_count"))
      end.compact

      my_import(thin_merchants)
    end
  end

  def update_yelp_reviews(params)
    self.update_attributes(:yelp_rating => params[:rating], :yelp_review_count => params[:review_count], :yelp_rating_image_url => params[:yelp_rating_image], :neighborhoods => (params[:neighborhoods] ? params[:neighborhoods].split(", ") : self.neighborhoods), :yelp_reviews => params[:reviews], :yelp_reviews_updated_at => Time.now, :yelp_id =>( self.yelp_id.present? ? self.yelp_id : params[:yelp_id]))
  end

  def update_city_reviews(params)
    self.update_attributes(:city_reviews => {rating: params[:rating], review_count: params[:review_count], reviews: params[:reviews]}, :neighborhoods => (params[:neighborhoods] ? params[:neighborhoods].split(", ") : self.neighborhoods), :city_reviews_updated_at => Time.now, parking: (self.parking.present? ? self.parking : params[:parking]), :payments => (self.payments.present? ? self.payments : params[:payments]), :hours => (self.hours.present? ? self.hours : params[:hours]))
  end

  def update_google_reviews(params)
    self.update_attributes(:google_reviews => params[:reviews], :neighborhoods => (params[:neighborhoods] ? params[:neighborhoods].split(", ") : self.neighborhoods), :google_reviews_updated_at => Time.now)
  end

end
