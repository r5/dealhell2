class ThinMerchantWriter < ActiveRecord::Base
  # self.rgeo_factory_generator = RGeo::Geos.method(:factory)

  serialize :location
  serialize :service_id
  serialize :category_id
  serialize :categories
  serialize :hours
  
  acts_as_mappable

  attr_accessor :lat, :lng, :lon

  belongs_to :service, :foreign_key => "main_service_id"

  if Rails.env.development?
    self.table_name = File.open(Rails.root.join("tmp","active_thin_write.txt"), "r")do |f| 
      name = f.read.chomp
      name.present? ? name : "thin_merchants"
    end
  else
    self.table_name = File.open(Rails.root.join("..", "..", "shared", "config", "active_thin_write.txt"), "r")do |f| 
      name = f.read.chomp
      name.present? ? name : "thin_merchants"
    end
  end

  def lng
    self.lon
  end

  def content
    self.name + " provides " + self.categories.join(", ")
  end

  def all_services
    Service.where(:id => self.service_id).map(&:name).join(", ")
  end

  def dealtitle(service_name=nil)
    service_name = service_name || self.service.name
    service_name + " from " + self.name
  end

  def slug_path(service_name = nil)
    service_name = service_name || self.service.name
    if self.state.present? and self.city.present?
      URI.escape("/tbusiness/#{service_name}/#{self.name}/#{self.id}")
    else
      URI.escape("/tbusiness/#{self.state}/#{self.city}/#{service_name}/#{self.name}/#{self.id}")
    end
  end

  def slug_url(service_name=nil)
    WEBSITE_NAME + slug_path(service_name)
  end

  def my_slug_path(service_name = nil, category_name=nil)
    service_name = service_name || self.service.name
    category_name = category_name || Category.find(self.category_id.first).name
    if self.state.present? and self.city.present?
      URI.escape("http://www.mytime.com/deals/#{self.state}/#{self.city}/#{category_name}/#{service_name}/#{self.name.gsub(" ", "-")}/mt-#{self.id_hash}")
    else
      ""
    end
  end

  def slug_url
    WEBSITE_NAME + slug_path
  end

def self.near(options={})
  mappable = options[:origin]
  match_ids= options[:match]
  per_page= options[:per_page] || 40
  url_type= URL_TYPES[options[:url_type]]
  page= options[:page] || 1
  service_id= options[:service_id]
  url_type = nil
  and_query = ""
  if match_ids.present?
    and_query << " and id in (#{match_ids.join(',')})"
  end
  if service_id
    and_query << " and main_service_id = #{service_id}"
  end
 
  sort_by = "geo_distance asc"
  bound = GeoKit::Bounds.from_point_and_radius(mappable,10)

sql = <<SQL
select *, GLength(LineStringFromWKB(LineString((latlon), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as geo_distance from thin_merchants where MBRWithin(latlon, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) #{and_query} order by #{sort_by}
SQL
  paginate_by_sql(sql, :page => page, :per_page => per_page)

end


  def self.import_another_file(filename = "thin_merchants.txt")
    categories = Category.all
    services = Service.all
    File.open("/home/vipin/scrapper/lib/scripts/scraping/facebook/thin_merchants_fb.txt").lines.each_slice(1000) do |lines|
      thin_merchants = lines.map do |line|
        line = JSON.parse(line.chomp)
        category = categories.find { |cat| cat.id == (line["category_id"] && line["category_id"].first) }
        service = services.find { |s| s.id == line["main_service_id"] }
        next if service.nil?
        line["id_hash"] = line["id"]
        line["lat"] = line["location"]["lat"]
        line["lon"] = line["location"]["lon"]
        line.delete("id")
        ThinMerchant.new(line.except("about", "fbid", "like_count", "checkin_count"))
      end.compact

      my_import(thin_merchants)
      puts "."
    end
  end

end
