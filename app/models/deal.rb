class Deal < ActiveRecord::Base

  include ActiveRecordOverride

# # By default, use the GEOS implementation for spatial columns.
# self.rgeo_factory_generator = RGeo::Geos.method(:factory)

# # But use a geographic implementation for the :latlon column.
# set_rgeo_factory_for_column(:latlon, RGeo::Geographic.spherical_factory)

 acts_as_mappable :lat_column_name => :lat,
                   :lng_column_name => :lon

 serialize :neighborhoods
 serialize :modifiers
 serialize :next_appointment_times

 belongs_to :service, :primary_key => :ass_id
  if Rails.env.development?
    if File.exist? Rails.root.join("tmp","active_deal_read.txt")
      self.table_name = File.open(Rails.root.join("tmp","active_deal_read.txt"), "r") do |f| 
        name = f.read.chomp
        name.present? ? name : "deals"
      end
    else
      self.table_name = "deals"
    end
  else
    if File.exist? Rails.root.join("..", "..", "shared", "config", "active_deal_read.txt")
      self.table_name = File.open(Rails.root.join("..", "..", "shared", "config", "active_deal_read.txt"), "r")do |f| 
        name = f.read.chomp
        name.present? ? name : "deals"
      end
    else
      self.table_name = "deals"
    end
  end
 DEAL_TYPES = {1 =>"Printable Coupon",  2 => "Tip",  3 => "Sale", 4 => "Special",  5 => "Always cheap",  6 => "Coupon Code", 7 => "Happy Hour", 8 => "Free Stuff", 18381 => "Deals-of-the-Day"}

 def dealtype
  if self.userid && self.userid == 18381
    "Deals of the Day"
  elsif self.dealtypeid
    DEAL_TYPES[self.dealtypeid]
  end
 end



  def slug_path(service_name = nil)
    service_name = service_name || self.service.name
    if self.state.present? and self.city.present?
      URI.escape("/deals/view/#{service_name}/#{CGI.escape(self.name)}/#{self.id}")
    else
      URI.escape("/deals/view/#{self.state}/#{self.city}/#{service_name}/#{CGI.escape(self.name)}/#{self.id}")
    end
  end

  def slug_url
    WEBSITE_NAME + slug_path
  end

  def showlogo
    (self.dealsource == "MyTime") ?  "my_logo.png" : self.attributes["showlogo"]
  end


  
  def lng
  self.lon
  end

 

def self.near(options={})
  mappable = options[:origin]
  match_ids= options[:match]
  per_page= options[:per_page] || 40
  # url_type= URL_TYPES[options[:url_type]]
  page= options[:page] || 1
  service_id= options[:service_id]
  match_name = options[:match_name]
  explain = options[:explain]
  distance = options[:distance] || 25
  # url_type = nil
  and_query = ""#url_type ? " and url_type = #{url_type}" : ""
  
  if service_id
    and_query << " and service_id = #{service_id}"
  end
  if options[:sort_by] == "pl"
    sort_by = "dealprice asc, geo_distance asc"
  elsif options[:sort_by] == "dist"
    sort_by = "geo_distance asc"
  elsif options[:sort_by] == "dis"
    sort_by = "dealdiscountpercent desc, geo_distance asc"
  elsif match_name.present?
    sort_by = "relevance , geo_distance asc"
  else
    sort_by = "geo_distance asc"
  end

  if match_name.present?
    and_query << " and MATCH(name) AGAINST (#{self.sanitize(match_name)} IN NATURAL LANGUAGE MODE)"
  end
  select_q = ""
  if match_name.present?
    select_q << " MATCH(name) AGAINST (#{self.sanitize(match_name)} IN NATURAL LANGUAGE MODE) as relevance,"
  end

  if !match_name.present?
    and_query << " and (expirationdate is null or expirationdate < #{Time.zone.now.to_date.to_s})"
  end
  bound = GeoKit::Bounds.from_point_and_radius(mappable, distance)

sql = <<SQL
#{explain ? 'explain' : ''} select *,#{select_q} GLength(LineStringFromWKB(LineString((latlon), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as geo_distance from #{Deal.table_name} where MBRWithin(latlon, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) #{and_query} order by #{sort_by}
SQL
deals = paginate_by_sql(sql, :page => page, :per_page => per_page, :includes => :services)
eagerLoadFor(deals, [:service])
deals
end


end