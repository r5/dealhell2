class SubCategory < ActiveRecord::Base

  belongs_to :category
  scope :enabled, -> { where(enabled: true) }

  def self.stuff_8_sub_categories(categories)
    SubCategory.create(categories)
  end
  
  def self.get_sub_categories_eight_coupons
    res = RestClient.get("http://api.8coupons.com/v1/getsubcategory")
    res = JSON.parse(res)
    res = res.map do |scat| 
      cat = Category.where(:name => scat["category"], :source => "8coupons").first
      {:name => scat["subcategory"], :enabled => true, :source_id => scat["subcategoryID"] , :source => "8coupons", :category_id => cat.id }
    end
    self.stuff_8_sub_categories(res)
  end

end
