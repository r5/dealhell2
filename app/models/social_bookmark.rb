class SocialBookmark < ActiveRecord::Base
  belongs_to :user
  belongs_to :my_attachment
  counter_culture :my_attachment
end
