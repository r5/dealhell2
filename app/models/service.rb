class Service < ActiveRecord::Base
  scope :enabled, -> { where(status: "enabled") }
  belongs_to :category

  serialize :ecoupon_service_ids
  serialize :mytime_service_id
  serialize :image_url, Array

  def self.mytime_service_from_file(filename)
    file = File.open(filename, "r")
    file.each_line do |l|
      l = l.split("|")
      st = Service.where(:name => l[1]).first
      if st
        st.update_attributes(:image_url => l.last.split(",,"))
        if st.id != l[0].to_i
          puts "Panga: #{st.id} --- #{s["name"]}"
        end
        if st.category_id != l[2].to_i
          puts "Panga Cat: #{st.id} --- #{s["name"]}"
        end
      end
    end
  end

  def self.add_ass_id_to_all_services
    Service.all.each do |s|
      s.update_attributes(:ass_id => s.id)
    end
  end
end

