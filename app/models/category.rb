class Category < ActiveRecord::Base
  has_many :services
  scope :enabled, -> { where(status: "enabled") }

  default_scope{ order("postion") }


  # def self.stuff_8_categories(categories)
  #   Category.create(categories)
  # end

  # def self.get_categories_eight_coupons
  #   res = RestClient.get("http://api.8coupons.com/v1/getcategory")
  #   res = JSON.parse(res)
  #   res = res.map{|cat| {:name => cat["category"], :enabled => true, :source_id => cat["categoryID"] , :source => "8coupons"} }
  #   self.stuff_8_categories(res)
  # end
end
