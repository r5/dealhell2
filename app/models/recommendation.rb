class Recommendation < ActiveRecord::Base

  belongs_to :user
  belongs_to :thin_merchant
  counter_culture :thin_merchant
end
