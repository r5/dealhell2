class MyAttachment < ActiveRecord::Base
  has_attached_file :asset, :hash_secret => "KyOpaTeWwTEG4Gay49wGmQZ/kBxexQEcNzx+MKG67oTZnGhQylp2Tpep6rH2ArVEsnbcpPHAmnxv3WRNVi5yOB6mTNmZh5+kkluFOjt674P1ZNr3zM7rrtHMa+JEmwAyRhkienUpJYqU4I+R/XW/FcQG1nUIjBwoebuqSF3F/QU=", :url => "/system/:attachment/:id/:style/:hash.:extension", :styles => {  :medium => "200x800>", :large => "443>x", :thumb => "60x60#"  }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/

  has_many :likes
  has_many :reposts
  has_many :social_bookmarkes
  belongs_to :pet
  has_many :comments

  # before_save :add_user_id


private
  # def add_user_id
  #   self.user_id = self.pet.owner_id
  # end

end
