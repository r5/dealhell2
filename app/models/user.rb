class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  validates_presence_of :user_name

  has_attached_file :avatar, :styles => {:thumb => "60x60#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  has_many :bookmarks
  has_many :recommendations
  has_many :pets, :foreign_key => "owner_id"
  has_many :likes
  has_many :social_bookmarks

  has_many :relationships, foreign_key: "follower_id", dependent: :destroy
  has_many :followings, through: :relationships, source: :followed

  def thumb
    image || avatar(:thumb)
  end

  def self.find_for_facebook_oauth(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.password = Devise.friendly_token[0,20]
        user.user_name = auth.info.name   # assuming the user model has a name
        # user.image = auth.info.image # assuming the user model has an image
    end
  end

 #  def self.from_omniauth(auth, current_user)
 #    authorization = Authorization.where(:provider => auth.provider, :uid => auth.uid.to_s, :token => auth.credentials.token, :secret => auth.credentials.secret).first_or_initialize
 #    if authorization.user.blank?
 #      user = current_user.nil? ? User.where('email = ?', auth["info"]["email"]).first : current_user
 #      if user.blank?
 #       user = User.new
 #       user.password = Devise.friendly_token[0,10]
 #       user.name = auth.info.name
 #       user.email = auth.info.email
 #       auth.provider == "twitter" ?  user.save(:validate => false) :  user.save
 #     end
 #     authorization.username = auth.info.nickname
 #     authorization.user_id = user.id
 #     authorization.save
 #   end
 #   authorization.user
 # end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def bookmark_thin_or_find(thin, bookmark_now=false)
    if bookmark_now
      book = self.bookmarks.find_or_initialize_by(:thin_merchant_id => thin.id)
      bookmark = book if book.new_record? && book.save
    else
      book = self.bookmarks.find_by(:thin_merchant_id => thin.id)
    end
    bookmarked =  book
    [bookmark, bookmarked]
  end

  def recommend_thin_or_find(thin, recommend_now=false)
    if recommend_now
      rec = self.recommendations.find_or_initialize_by(:thin_merchant_id => thin.id)
      recommendation = rec if rec.new_record? && rec.save
    else
      rec = self.recommendations.find_by(:thin_merchant_id => thin.id)
    end
    recommended =  rec 
    [recommendation, recommended]
  end

  def following?(pet)
    relationships.find_by(followed_id: pet.id)
  end

  def follow!(pet)
    relationships.create!(followed_id: pet.id)
  end

  def unfollow!(pet)
    relationships.find_by(followed_id: pet.id).destroy
  end
end
