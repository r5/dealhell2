class DealWriter < ActiveRecord::Base

  include ActiveRecordOverride

# # By default, use the GEOS implementation for spatial columns.
# self.rgeo_factory_generator = RGeo::Geos.method(:factory)

# # But use a geographic implementation for the :latlon column.
# set_rgeo_factory_for_column(:latlon, RGeo::Geographic.spherical_factory)

 acts_as_mappable :lat_column_name => :lat,
                   :lng_column_name => :lon

 belongs_to :service
  if Rails.env.development?
    if File.exist? Rails.root.join("tmp","active_deal_write.txt")
      self.table_name = File.open(Rails.root.join("tmp","active_deal_write.txt"), "r")do |f| 
        name = f.read.chomp
        name.present? ? name : "mdeals"
      end
    else
      self.table_name = "mdeals"
    end
  else
    if File.exist? Rails.root.join("..", "..", "shared", "config", "active_deal_write.txt")  
      self.table_name = File.open(Rails.root.join("..", "..", "shared", "config", "active_deal_write.txt"), "r")do |f| 
        name = f.read.chomp
        name.present? ? name : "mdeals"
      end
    else
      self.table_name = "mdeals"
    end
  end

 DEAL_TYPES = {1 =>"Printable Coupon",  2 => "Tip",  3 => "Sale", 4 => "Special",  5 => "Always cheap",  6 => "Coupon Code", 7 => "Happy Hour", 8 => "Free Stuff", 18381 => "Deals-of-the-Day"}

 def dealtype
  if self.userid && self.userid == 18381
    "Deals of the Day"
  elsif self.dealtypeid
    DEAL_TYPES[self.dealtypeid]
  end
 end

 def self.stuff_8_coupons_deals(json)
   my_import(json)
 end


  def slug_path(service_name = nil)
    service_name = service_name || self.service.name
    if self.state.present? and self.city.present?
      URI.escape("/deals/view/#{service_name}/#{self.name}/#{self.id}")
    else
      URI.escape("/deals/view/#{self.state}/#{self.city}/#{service_name}/#{self.name}/#{self.id}")
    end
  end

  def slug_url
    WEBSITE_NAME + slug_path
  end

  def showlogo
    (self.dealsource == "MyTime") ?  "my_logo.png" : self.attributes["showlogo"]
  end

  def self.get_mytime_rating_and_review(deal)
    if deal["yelp_rating"].present? && deal["yelp_rating"].to_i > 0
      [deal["yelp_rating"].to_i.to_f, deal["yelp_review_count"].to_i]
    elsif deal["google_rating"].present? && deal["google_rating"].to_i > 0
      [deal["google_rating"].to_i.to_f, deal["google_review_count"].to_i]
    elsif deal["city_rating"].present? && deal["city_rating"].to_i > 0
      [deal["city_rating"].to_i.to_f, deal["city_review_count"].to_i]
    elsif deal["mytime_rating"].present? && deal["mytime_rating"].to_i > 0
      [deal["mytime_rating"].to_i.to_f, deal["mytime_review_count"].to_i]
    else
      [nil, nil]
    end  
  end 

 def self.stuff_mytime_deals(json_arr)
  services = {}
  Service.all.each{|s| services[s.name] = s.ass_id }
  cats ={}
  url_types = {}
  Service.all.each do|c| 
    cats[c.name] = c.category_id
  end
  Category.all.each do |c|
    url_types[c.id] = c.url_type
  end
  json_arr =json_arr.map do |json|
    begin
    mdeal = JSON.parse(json)
    next if !mdeal["location"]["lat"].present? || !mdeal["location"]["lon"].present?
    next unless services[mdeal["service_name"]].present?
    category_id = cats[mdeal["service_name"]]
    rating, review_count = DealWriter.get_mytime_rating_and_review(mdeal)
    url_type = URL_TYPES[url_types[cats[mdeal["service_name"]]]]  
    {
      "affiliate" =>  true,
      "homepage" => mdeal["website"],
      "name" =>  mdeal["name"],
      "address" =>  mdeal["street_address"],
      "city" =>  mdeal["city"],
      "state" =>  mdeal["state"],
      "zip" =>  mdeal["zip"],
      "phone" =>  mdeal["phone_number"],
      "url" =>  "http://mytime.com" + mdeal["slug_path"],
      "avg_rating" => rating,
      "showlogo" => "my_logo.png",
      "review_count" => review_count,
      "dealsource" =>  "MyTime",
      "source_deal_id" =>  mdeal["id"],
      "dealtitle" =>  mdeal["service_name"] + " from " + mdeal["name"],
      "dealinfo" =>  mdeal["company"]["description"],
      "showimagestandardsmall" =>  mdeal["photo_url"],
      "showimagestandardbig" =>  mdeal["photo_url"].gsub("slate_black", "medium"),
      "showimage" =>  mdeal["photo_url"].gsub("slate_black", "medium"),
      "service_id" => services[mdeal["service_name"]],
      "category_id" =>  category_id,
      "url_type" => url_type,
      "lat" =>  mdeal["location"]["lat"],
      "lon" =>  mdeal["location"]["lon"],
      "dealoriginalprice" =>  mdeal["max_price"],
      "dealprice" =>  mdeal["min_price"],
      "dealdiscountpercent" =>  ((1-(mdeal["min_price"].to_f/mdeal["max_price"].to_f))*100).floor,
      "my_deal_type" =>  "local",
      "modifiers" =>  mdeal["modifiers_values"],
      "next_appointment_times" =>  mdeal["next_appointment_times"],
      "neighborhoods" =>  mdeal["company"]["locations"].find{|l| l["id"].to_i == mdeal["id"].split("-").last.to_i }.try{|m| m["neighborhoods"]} || [] ,
      "latlon" =>  "GeomFromText('Point(#{mdeal["location"]["lat"]} #{mdeal["location"]["lon"]})')"
    }
    rescue Exception => e
      puts e.message
    end
  end
  json_arr = json_arr.compact
  my_import(json_arr.compact) if json_arr.present?
 end

  def self.get_mytime_deals
    results = RestClient.get("http://localhost:3000/api/v1/deals/trenato_deals?page=1&per_page=100")
    results = JSON.parse(results)
    stuff_mytime_deals(results["deals"])
    (results["total_pages"] - 1).times do |page|
      results = RestClient.get("http://localhost:3000/api/v1/deals/trenato_deals?page=#{page+2}&per_page=100")
      results = JSON.parse(results) 
      Deal.stuff_mytime_deals(results["deals"])
    end
  end

 def lng
  self.lon
 end

 



def self.keep_page_number_in_file(filename, number)
  if Rails.env.development?
    File.open(Rails.root.join("tmp", filename), "w") { |file| file.write number }
  else
    File.open(Rails.root.join("..", "..", "shared", "config", filename), "w") { |file| file.write number }
  end
end

  def self.read_previous_page_from_file(filename)
    if Rails.env.development?
      myfilepath = Rails.root.join("tmp", filename)
    else
      myfilepath = Rails.root.join("..", "..", "shared", "config", filename)
    end
    if File.exists? myfilepath
      File.open(myfilepath, "r") { |file| file.read.chomp.to_i }
    else
      1
    end
  end

  def self.switch_deal_reader_table
    if Rails.env.development?
      mywritefilepath = Rails.root.join("tmp", "active_deal_write.txt")
      myreadfilepath = Rails.root.join("tmp", "active_deal_read.txt")
    else
      mywritefilepath = Rails.root.join("..", "..", "shared", "config", "active_deal_write.txt")
      myreadfilepath = Rails.root.join("..", "..", "shared", "config", "active_deal_read.txt")
    end
    File.open(mywritefilepath, "w"){|f| f.write Deal.table_name}
    File.open(myreadfilepath, "w"){|f| f.write  DealWriter.table_name}
  end


  def self.remove_utf_8_four_bytes(text, key)
    if ["name","address", "title","disclaimer","dealinfo", "dealtitle"].include?(key)
     text = text.each_char.select{|c| c.bytes.count < 4 }.join('')
    end
    text
  end

  def self.get_8_coupons_today
    services = Service.all
    cats ={}
    url_types = {}
    offset = read_previous_page_from_file("8_coupons_previous_page")
    Category.all.each do |c|
      url_types[c.id] = c.url_type
    end
    500.times do |i|
      begin
      # puts "http://api.8coupons.com/v1/getrealtimelocaldeals?key=#{EIGHT_COUPONS}&page=#{i+41}&limit=1000"
      # puts "http://api.8coupons.com/v1/getrealtimechaindeals?key=#{EIGHT_COUPONS}&page=#{i+1}&limit=1000"
      # puts "http://api.8coupons.com/v1/getrealtimeproductdeals?key=#{EIGHT_COUPONS}&page=#{i+1}"
      cpage = i+offset
      res = RestClient.get("http://api.8coupons.com/v1/getrealtimelocaldeals?key=#{EIGHT_COUPONS}&page=#{cpage}&limit=1000")
      break if res.match(/no deal return/)
      res = JSON.parse(res)
      puts res.length
      res = res.map do |deal|
        next if !deal["lat"].present? || !deal["lon"].present?
        formatted = {}
        deal.keys.each do |key|
          formatted[key.downcase] = self.remove_utf_8_four_bytes(deal[key], key.downcase)
        end
        scat = services.find{|sc| sc.ecoupon_service_ids && sc.ecoupon_service_ids.include?(formatted["subcategoryid"].to_i)}
        next unless scat
        source_deal_id = formatted["id"]
        formatted.delete("id")
        formatted["source_deal_id"] = source_deal_id
        formatted["affiliate_source"] = "8coupons"
        formatted["service_id"] = scat.ass_id.to_s
        formatted["url_type"] = url_types[scat.category_id]
        formatted["category_id"] = scat.category_id
        formatted["my_deal_type"] = "local"
        formatted["affiliate"] = formatted["affiliate"].downcase == "yes" if formatted["affiliate"] 
        formatted["latlon"] =  "GeomFromText('Point(#{formatted["lat"]} #{formatted["lon"]})')"
        formatted
      end
      stuff_8_coupons_deals(res.compact) if res.present?
      rescue Exception => e
        keep_page_number_in_file("8_coupons_previous_page", (cpage-1).to_s)
        raise e
      end
    end
    keep_page_number_in_file("8_coupons_previous_page", (1).to_s)
  end
end