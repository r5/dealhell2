class ZipCode < ActiveRecord::Base

  acts_as_mappable

  def self.autocomplete(query)
    if query.present?
      # query = query.squeeze(" ").split(" ").map{|q| "+" + q}.join(" ")
      find_by_sql("select * from zip_codes where MATCH(city, state, zip, county) AGAINST (#{self.sanitize(query)} IN NATURAL LANGUAGE MODE) limit 20").map{|zip| "#{zip.city}, #{zip.state} #{zip.zip}"}.to_json
    else
      [].to_json
    end
  end

  def self.text_search(query)
    if query.present?
      find_by_sql("select * from zip_codes where MATCH(city, state, zip, county) AGAINST (#{self.sanitize(query)} IN NATURAL LANGUAGE MODE) limit 20")
    else
      []
    end
  end

  def self.near(lat, lng)
    zip = ZipCode.new(:lat => lat, :lng => lng)
    mappable = zip
    distance = 5 
    sort_by = "geo_distance asc"
    bound = GeoKit::Bounds.from_point_and_radius(mappable, distance)
    sql = <<SQL
select *, GLength(LineStringFromWKB(LineString((latlon), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as geo_distance from zip_codes where MBRWithin(latlon, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) order by #{sort_by}
SQL
  zips = find_by_sql(sql)
  end
end
