class Product < ActiveRecord::Base

def self.stuff_8_coupons_products(json)
  import(json)
end

def self.get_8_coupons_today
  services = Service.all
  cats ={}
  url_types = {}
  Category.all.each do |c|
    url_types[c.id] = c.url_type
  end
  40.times do |i|
    res = RestClient.get("http://api.8coupons.com/v1/getrealtimeproductdeals?key=#{EIGHT_COUPONS}&page=#{i+1}&limit=1000  ")
    res = JSON.parse(res)
    puts res.length
    res = res.map do |deal|
      formatted = {}
      deal.keys.each do |key|
        formatted[key.downcase] = deal[key]
      end
      scat = services.find{|sc| sc.ecoupon_service_ids && sc.ecoupon_service_ids.include?(formatted["subcategoryid"].to_i)}
      next unless scat
      source_product_id = formatted["id"]
      formatted.delete("id")
      formatted["product_source_id"] = source_product_id
      formatted["affiliate_source"] = "8coupons"
      formatted["service_id"] = scat.id.to_s
      formatted["url_type"] = url_types[scat.category_id]
      formatted["category_id"] = scat.category_id
      formatted["my_deal_type"] = "shop"
      formatted
    end
    stuff_8_coupons_products(res.compact)
  end
 end
end
