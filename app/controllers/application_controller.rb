class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include UrlHelper

  before_filter :configure_permitted_parameters, if: :devise_controller?
  after_filter :store_location
  before_filter :set_my_location_from_ip
  before_filter :basic_authenticate
  layout :layout_by_resource


  def set_my_location_from_ip
    if !cookies[:lat] && !cookies[:lan] && !session[:mxmind_tried]
      db = MaxMindDB.new(File.expand_path(Rails.root) + '/GeoLite2-City.mmdb')
      ret = db.lookup(Rails.env.production?  ? request.ip : '23.23.111.203');
      if ret
        zip = ZipCode.near(ret['location']['latitude'], ret['location']['longitude']).first
        if zip
          set_location_by_zip(zip)
        end
      end
      session[:mxmind_tried] =  true
    end
  end


  def set_my_location(location_query)
    if location_query.present?
      zip = location_query.match(/(\d\d\d\d\d)/).captures[0] if location_query.match(/(\d\d\d\d\d)/)
      locations = ZipCode.text_search(location_query)
      if locations.length == 1
        location = locations.first
      elsif zip
        location = ZipCode.where(:zip => zip).first
      elsif locations.first
        location = locations.first
      else
        location = ZipCode.find(37493) # Defaulting to first zip in Los Angeles. Use ID so that index on table is not required
      end
    else
      location = ZipCode.find(37493)
    end
    set_location_by_zip(location)
  end

  def set_location_by_zip(zip)
    cookies.permanent[:mzip] = zip.zip
    cookies.permanent[:lat] = zip.lat
    cookies.permanent[:lng] = zip.lng
    cookies.permanent[:city] = zip.city
    cookies.permanent[:state] = zip.state
    cookies.permanent[:location] = "#{zip.city}, #{zip.state} #{zip.zip}"
    cookies.permanent[:location] = params[:location] if params[:location]
  end

  protected

  def basic_authenticate
    if Rails.env != 'development'
      authenticate_or_request_with_http_basic "Sqooz! We Are Coming Soon." do |username, password|
        username == "sqooz" && password == "sqooz"
      end
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:user_name, :avatar]
  end


  def store_location
  # store last url - this is needed for post-login redirect to whatever the user last visited.
    if (request.fullpath != "/users/sign_in" &&
      request.fullpath != "/users/sign_up" &&
      request.fullpath != "/users/password" &&
      request.fullpath != "/users/sign_out" &&
      !request.xhr?) # don't store ajax calls
      session[:previous_url] = request.fullpath 
    end
  end

  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end
end
