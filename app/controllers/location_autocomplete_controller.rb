class LocationAutocompleteController < ApplicationController

  def index
    render :json => ZipCode.autocomplete(params[:q]), :state => 200
  end

end
