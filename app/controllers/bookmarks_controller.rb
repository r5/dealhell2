class BookmarksController < ApplicationController
  
  before_filter :authenticate_user!

  def index
    @bookmarks = current_user.bookmarks.paginate(:page => params[:page] || 1, :per_page => 40).order("created_at desc")
    @total_entries = @bookmarks.total_entries
    @total_pages = @bookmarks.total_pages
  end

  def create
  end

  def destroy
    bookmark = Bookmark.find_by_id(params[:id])
    if bookmark and bookmark.user_id == current_user.id
      bookmark.destroy
    end
    redirect_to :back, :flash => {:notice => "Unbookmarked Successfully!"}
  end
end
