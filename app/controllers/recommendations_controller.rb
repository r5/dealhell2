class RecommendationsController < ApplicationController
 
  before_filter :authenticate_user!
  before_filter :recommendation_params , :only => [:update]

  def index
    @recommendations = current_user.recommendations.paginate(:page => params[:page] || 1, :per_page => 40).order("created_at desc").includes(:thin_merchant)
    @total_entries = @recommendations.total_entries
    @total_pages = @recommendations.total_pages
  end

  def create

  end

  def update
    recommendation = Recommendation.find_by_id(params[:id])
    respond_to do |format|
      if recommendation and recommendation.user_id == current_user.id && recommendation.update_attributes(params[:recommendation])
        format.html { redirect_to recommendation.thin_merchant.slug_path, notice: 'Recommendation was successfully updated.' }
        format.js { render :js => "window.location = window.location.href"}
      else
        format.html { redirect_to recommendation.thin_merchant.slug_path, notice: 'There was an error updating recommendation' }
        format.js { render '$("#recommendation_text_modal form textarea").next("p.t_error").remove(); $("#recommendation_text_modal form textarea").after("<p class=\'t_error\'>Ooops!! There was an error.</p>")' }
      end
    end
  end

  def destroy
    recommendation = Recommendation.find_by_id(params[:id])
    if recommendation and recommendation.user_id == current_user.id
      recommendation.destroy
    end
    redirect_to :back, :flash => {:notice => "Successfully destroyed recommendation!"}
  end

  private

  def recommendation_params
    params[:recommendation] = params.require(:recommendation).permit(:rtext)
  end
end
