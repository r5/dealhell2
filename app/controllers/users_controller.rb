class UsersController < ApplicationController
  layout "socialization"
  before_filter :authenticate_user!, :only => ["pets"]

  def pets
    @pets = current_user.pets.includes({:album => :my_attachments})
  end

end
