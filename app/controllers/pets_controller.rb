class PetsController < ApplicationController
  layout "socialization"

  def index
    @albums = Album.limit(20)
  end

  def show
    @pet = Pet.first
    @album = @pet.albums.first
  end

end
