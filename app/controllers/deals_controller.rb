class DealsController < ApplicationController

  # before_filter :set_my_location, :only => [:index]
  before_filter :set_my_location_if_wrong, :only => [:index]
  # before_filter :normalize_location, :only => [:index]


  def index
    per_page_thin = 24
    if request.path != "/search" || !params[:merchant].present?  # Search is no url type, just a convention
      @service = Service.where(:name => params[:service]).first if params[:service]
      @thins, @total_entries, @total_pages = ThinMerchant.near(origin: ThinMerchant.new(:lat => cookies[:lat] || "37.7933", :lon => cookies[:lng] || "-122.4212"), :per_page => per_page_thin, :service_id => @service.try(:id), :distance => params[:zip] ? 40 : nil, :page => params[:page] || 1)
      if params[:service] && params[:city] && [:state] && !params[:zip]
        @canonical = zip_service_deals_url(:zip => cookies[:mzip], :city => cookies[:city], :state => cookies[:state], :service => params[:service])
      elsif params[:city] && [:state] && !params[:zip]
        @canonical = zip_deals_url(:zip => cookies[:mzip], :city => cookies[:city], :state => cookies[:state] )
      end 
      # end
    elsif request.path == "/search"
      set_my_location(params[:location])
      @thins, @total_entries, @total_pages = ThinMerchant.near(origin: ThinMerchant.new(:lat => cookies[:lat] || "37.7933", :lon => cookies[:lng] || "-122.4212"), :per_page => 30, :service_id => @service.try(:id), :match_name => params[:merchant], :distance => 25, :page => params[:page] || 1)
    end
    if current_user
      @recommendations = current_user.recommendations.map(&:thin_merchant_id)
      @bookmarks = current_user.bookmarks.map(&:thin_merchant_id)
    end
    # render "index_2"
  end

  def show
    @deal = Deal.where(:id => params[:id]).first
    if @deal
      flash.now[:success] = "#{@deal.name} has been bookmarked!" if current_user && params[:bookmark] && current_user.bookmark_thin(@deal)
      flash.now[:success] = "You have successfully recommended #{@deal.name}" if current_user && params[:recommend] && current_user.recommend_thin(@deal)
      render "show"
    else
      begin
        redirect_to :back, :flash => { :error => "Sorry! Could not find the deal." }
      rescue ActionController::RedirectBackError
        redirect_to root_path, :flash => { :error => "Sorry! Could not find the deal." }
      end
    end
  end

  # def get_zip
  #   # if set_my_location(params[:mzip])
  #   #   rpath = deals_path(:state => cookies[:state], :city => cookies[:city])
  #   #   render :js => "window.location = '#{rpath}'"
  #   # else
  #   #   render :js => '$("#zip_collector form input.zipcode").next("p.t_error").remove(); $("#zip_collector form input.zipcode").after("<p class=\'t_error\'>ZIP not found! Please try a different ZIP.</p>")'
  #   # end
  # end

private


  def set_my_location_if_wrong
    if params[:city].present? && cookies[:city] != params[:city]
      set_my_location("#{params[:city]}, #{params[:state]}")
    end
  end

end
