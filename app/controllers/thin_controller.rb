class ThinController < ApplicationController


  def show
    @thin = ThinMerchant.where(:id => params[:id]).first
    if @thin
      set_up_recommendation_and_bookmark
      @login_modal = true if !current_user && (params[:recommend] || params[:bookmark])
      @rating , @review_count, @review_type = @thin.review_and_rating
      render "show"
    else
      begin
        redirect_to :back, :flash => { :error => "Sorry! Coulddef  not find the business." }
      rescue ActionController::RedirectBackError
        redirect_to root_path, :flash => { :error => "Sorry! Could not find the business." }
      end
    end
  end

  def update_reviews
    @thin = ThinMerchant.where(:id => params[:id]).first
    if @thin
      if params["review_type"] == "yelp"
        @thin.update_yelp_reviews(params)
      elsif params["review_type"] == "google"
        @thin.update_google_reviews(params)
      elsif params["review_type"] == "city"
        @thin.update_city_reviews(params)
      end
      render :text => "0k", :status => 200
    else
      render :text => "not_found", :status => 404
    end
  end

private

def set_up_recommendation_and_bookmark
  if current_user
    @bookmark, @bookmarked = current_user.bookmark_thin_or_find(@thin, params[:bookmark])
    @recommendation, @recommended = current_user.recommend_thin_or_find(@thin, params[:recommend])
  end
  flash.now[:notice] = "#{@thin.name} has been successfully bookmarked!" if @bookmark
  flash.now[:notice] = "Successfully recommended #{@thin.name}" if @recommendation
end

end
