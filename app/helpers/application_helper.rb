module ApplicationHelper

  def tweet_me(url, text, link_text="")
    link_to "Twitter", "https://twitter.com/share?", :"data-url" => url, :"data-text" => text, :onclick => "return tweet_click($(this))", :class => "shared-item twitter"
  end

  def facebook_sharer(url, title="", link_text="", fclass="")
    "<a rel='nofollow' class='#{fclass} shared-item facebook' href='http://www.facebook.com/share.php' data-url='#{url}' data-title='#{title}' onclick='return fbs_click($(this))' target='_blank'>Facebook</a>".html_safe
  end

  def dstate
    cookies[:state] || "CA"
  end

  def dcity
    cookies[:city] || "Los Angeles"
  end

  def dzip
    cookies[:mzip] || "90005"
  end

  def link_to_iframe(text, path, params={})
    link_to(text, myframe_path(:to => URI.escape(path)), :class => params[:class])
  end

  def flash_class(level)
    case level
    when :notice then "alert-info"
    when :error then "alert-danger"
    when :alert then "alert-warning"
    end
  end
end
