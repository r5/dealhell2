module ThinHelper

  def thin_region_details(thin)
    db_zip = ZipCode.where(:zip => thin.zip.to_s).first if !thin.city.present? || !thin.state.present?
    city = thin.city.present? ? thin.city : db_zip.try(&:city)
    state = thin.state.present? ? thin.state : db_zip.try(&:state)
    return (city ? ("<span class='medium_text' itemprop='addressLocality'>" + city + "</span>").html_safe + ", " : "") + (("<span class='medium_text' itemprop='addressRegion'>" + state + "</span>").html_safe || "") + " " + ( "<span class='medium_text' itemprop='postalCode'>" + thin.zip.to_s + "</span>").html_safe
  end
end
