module DealsHelper

  def merge_with_deals_path(additional)
    if params[:service]
      service_deals_path({:service => params[:service], :state => params[:state] || dstate, :city => params[:city] || dcity}.merge(additional))
    else
      deals_path({:state => params[:state] || dstate, :city => params[:city] || dcity}.merge(additional))
    end
  end

  def prev_page
    return nil unless (params[:page] && params[:page].to_i > 1)
    cpage = params[:page]
    if params[:service]
      service_deals_path({:service => params[:service], :page => cpage.to_i - 1, :state => params[:state] || dstate, :city => params[:city] || dcity})
    else
      deals_path({:page => cpage.to_i - 1, :state => params[:state] || dstate, :city => params[:city] || dcity})
    end
  end

  def round_to_point_five(number)
    r = (number.to_f * 2).round / 2.0
  end

  def all_ratings(thin, hide_logo_xs=true)
    # ratings = '<ul class="list-unstyled">'
    # ratings += ("<li>" + yelp_rating(thin) + "</li>") if thin.yelp_id.present? && thin.yelp_rating > 0
    # # ratings += ("<li>" + city_rating(thin) + "</li>") if thin.city_reviews.present? and thin.city_reviews[:rating] and thin.city_reviews[:rating].to_i > 0
    # ratings += "</ul>"
    ratings = yelp_rating(thin)
    ratings.html_safe
  end

  def yelp_rating(thin)
    if thin.yelp_rating.to_f > 0
      "<div class='my-small-rating'><a  target='_blank' href='http://yelp.com/biz/#{thin.yelp_id}' ><ul class='list-inline'><li><img src='#{YELP_RATING_TO_IMAGE[thin.yelp_rating.to_f]}' alt='#{thin.yelp_rating}'></li><li class='text-muted'>[#{thin.yelp_review_count}]</li><li>#{image_tag "yelp_icon@2x.png", :width => 42, :id => 'yelp_rating_image'}</li></ul></a></div>".html_safe
    else
      nil
    end
  end

  def city_rating(thin)
    if thin.city_reviews.present? && thin.city_reviews.class != Array && thin.city_reviews[:review_count].to_i > 0
      "<div class='my-small-rating'><a target='_blank' href='#{thin.city_reviews[:profile_url]}' ><ul class='list-unstyled'><li class='diblock citysearch stars_#{thin.city_reviews[:rating]} small'></li><li class='diblock text-muted'>[#{thin.city_reviews[:review_count]}]</li><li class='diblock'>#{image_tag "citysearch.jpg",  :id => 'city_rating_image'}</li></ul></a></div>".html_safe
    elsif thin.city_reviews.present? && thin.city_reviews.class == Array && thin.city_reviews.first[:review_count].to_i > 0
      rev = thin.city_reviews.first
      "<div class='my-small-rating'><a target='_blank' href='#{rev[:url]}' ><ul class='list-unstyled'><li class='diblock'><span class='citysearch stars_#{rev[:avg_rating]} small diblock'></span></li><li class='diblock text-muted'>[#{rev[:review_count]}]</li><li class='diblock'>#{image_tag "citysearch.jpg",  :id => 'city_rating_image'}</li></ul></a></div>".html_safe
    else
      nil
    end
  end

  def next_page
    cpage = params[:page] || 1
    if params[:service]
      service_deals_path({:state => params[:state] || dstate, :city => params[:city] || dcity, :service => params[:service], :page => cpage.to_i + 1})
    else
      deals_path({:page => cpage.to_i + 1, :city => params[:city] || dcity, :state => params[:state] || dstate})
    end
  end
end
