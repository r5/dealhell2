class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.integer :actor_id
      t.string :message
      t.boolean :read
      t.string :slug_path
      t.integer :pet_id
      t.integer :action_id
      t.string :action_type

      t.timestamps
    end
    add_index :notifications, :user_id
    add_index :notifications, :actor_id
    add_index :notifications, :pet_id
    # TODO Add the index below if you ever need it
    # add_index :notifications, [:action_id, :action_type]
  end
end
