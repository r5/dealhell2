class CreateRecommendations < ActiveRecord::Migration
  def change
    create_table :recommendations do |t|
      t.integer :user_id
      t.integer :thin_merchant_id
      t.text :rtext
      t.boolean :published, :default => false

      t.timestamps
    end
    add_index :recommendations, :user_id
    add_index :recommendations, :thin_merchant_id
  end
end
