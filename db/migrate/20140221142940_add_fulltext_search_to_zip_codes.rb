class AddFulltextSearchToZipCodes < ActiveRecord::Migration
  def up
    # Always add ft_min_word_len=3 to my,cnf to make it work with 3 charaters
    execute 'alter table zip_codes engine myisam'
    execute "CREATE FULLTEXT INDEX fulltext_location ON zip_codes (city, state, zip, county)"
  end

  def down
    execute "ALTER TABLE zip_codes DROP INDEX fulltext_location"
  end
end
