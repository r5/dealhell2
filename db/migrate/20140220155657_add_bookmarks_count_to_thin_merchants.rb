class AddBookmarksCountToThinMerchants < ActiveRecord::Migration

  def self.up

    add_column :thin_merchants, :bookmarks_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :thin_merchants, :bookmarks_count

  end

end
