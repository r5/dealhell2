class AddMyAttachmentIdToLikes < ActiveRecord::Migration
  def change
    add_column :likes, :my_attachment_id, :integer
    add_index :likes, :my_attachment_id
  end
end
