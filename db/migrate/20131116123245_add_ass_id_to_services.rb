class AddAssIdToServices < ActiveRecord::Migration
  def up
    add_column :services, :ass_id, :integer
    add_index :services, :ass_id
    Service.add_ass_id_to_all_services
  end

  def down
    remove_column :services, :ass_id
  end
end
