class AddServiceIdToDeals < ActiveRecord::Migration
  def change
    add_column :deals, :service_id, :integer
  end
end
