class AddSpatialIndexToZipCode < ActiveRecord::Migration
  def up
    execute "alter table zip_codes add latlon point not null"
    execute 'update zip_codes set latlon = GeomFromText(concat(\'Point(\',lat,\' \',lng,\')\'))'
    execute "create spatial index thin_lat_lon on zip_codes (latlon)"
  end

  def down
  end
end
