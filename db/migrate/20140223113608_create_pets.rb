class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.string :name
      t.string :breed
      t.integer :likes_count
      t.integer :reposts_count
      t.text :about_pet
      t.integer :owner_id

      t.timestamps
    end
  end
end
