class CreateMyAttachments < ActiveRecord::Migration
  def change
    create_table :my_attachments do |t|
      t.integer :user_id
      t.integer :pet_id

      t.timestamps
    end
  end
end
