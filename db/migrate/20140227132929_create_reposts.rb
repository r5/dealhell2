class CreateReposts < ActiveRecord::Migration
  def change
    create_table :reposts do |t|
      t.integer :user_id
      t.integer :my_attachment_id
      t.timestamps
    end
    add_index :reposts, :user_id
    add_index :reposts, :my_attachment_id
  end
end
