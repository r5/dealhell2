class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :title
      t.text :about
      t.integer :user_id
      t.integer :pet_id

      t.timestamps
    end
  end
end
