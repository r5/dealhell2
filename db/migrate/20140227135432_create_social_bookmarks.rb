class CreateSocialBookmarks < ActiveRecord::Migration
  def change
    create_table :social_bookmarks do |t|
      t.integer :user_id
      t.integer :my_attachment_id

      t.timestamps
    end
    add_index :social_bookmarks, :user_id
    add_index :social_bookmarks, :my_attachment_id
  end
end
