class AddIndexToBookmarks < ActiveRecord::Migration
  def change
    add_index :bookmarks, :user_id
    add_index :bookmarks, :thin_merchant_id
  end
end
