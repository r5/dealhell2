class AddAlbumIdToMyAttachments < ActiveRecord::Migration
  def change
    add_column :my_attachments, :album_id, :integer
  end
end
