class MakeThinMyism < ActiveRecord::Migration 
  def self.up 
    execute 'alter table thin_merchants add latlon point not null'
    execute 'alter table thin_merchants engine myisam'
    execute "update thin_merchants set lon = REPLACE(SUBSTRING_INDEX( location , '\nlon: ', -1 ), '\n', '')"
    #   
    execute "update thin_merchants set lat = REPLACE(REPLACE(SUBSTRING_INDEX(location , '\nlon: ', 1 ), '\n', ''),'---lat: ', '')"
    execute "update thin_merchants set latlon = GeomFromText(concat(\'Point(\',lat,\' \',lon,\')\'))"
    execute "create trigger set_latlon_thin_point before insert on thin_merchants for each row set new.latlon = GeomFromText(concat('Point(',new.lat,' ',new.lon,')'));" 
    execute 'create spatial index thin_lat_lon on thin_merchants(latlon)' 
    execute "CREATE FULLTEXT INDEX fulltext_thin_name ON thin_merchants (name)"
  end 
  def self.down 
    execute "ALTER TABLE thin_merchants DROP INDEX fulltext_thin_name"
  end
end

# SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( "---\nlat: 39.065036\nlon: -84.284998\n" , '---\nlat: ', 2 ),'\nlon: ',1), '\nlat: ', 2)
