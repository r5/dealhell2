class AddCompanyDetailToDeals < ActiveRecord::Migration
  def change
    add_column :deals, :company_detail, :text
    add_column :mdeals, :company_detail, :text
  end
end
