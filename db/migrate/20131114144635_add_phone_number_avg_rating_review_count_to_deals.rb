class AddPhoneNumberAvgRatingReviewCountToDeals < ActiveRecord::Migration
  def change
    add_column :deals, :phone_number, :string
    add_column :deals, :avg_rating, :float
    add_column :deals, :review_count, :integer
    add_column :mdeals, :phone_number, :string
    add_column :mdeals, :avg_rating, :float
    add_column :mdeals, :review_count, :integer
  end
end
