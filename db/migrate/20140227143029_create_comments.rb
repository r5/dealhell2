class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.integer :my_attachment_id
      t.text :comment_text
      t.boolean :spam
      
      t.timestamps
    end
    add_index :comments, :user_id
    add_index :comments, :my_attachment_id
  end
end
