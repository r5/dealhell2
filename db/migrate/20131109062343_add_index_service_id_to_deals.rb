class AddIndexServiceIdToDeals < ActiveRecord::Migration
  def change
    add_index :deals, :service_id
  end
end
