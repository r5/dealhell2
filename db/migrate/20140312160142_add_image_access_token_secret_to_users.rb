class AddImageAccessTokenSecretToUsers < ActiveRecord::Migration
  def change
    add_column :users, :image, :string
    add_column :users, :access_toke, :string
    add_column :users, :secret, :string
  end
end
