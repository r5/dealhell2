class AddLikesCountToMyAttachments < ActiveRecord::Migration

  def self.up

    add_column :my_attachments, :likes_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :my_attachments, :likes_count

  end

end
