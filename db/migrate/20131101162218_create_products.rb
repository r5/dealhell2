class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.boolean :affiliate
      t.string :dealsource
      t.string :user
      t.integer :product_source_id
      t.string :dealtitle
      t.string :showimage
      t.date :expirationdate
      t.datetime :postdata
      t.string :storeurl
      t.integer :chainid
      t.integer :userid
      t.string :name

      t.timestamps
    end
  end
end
