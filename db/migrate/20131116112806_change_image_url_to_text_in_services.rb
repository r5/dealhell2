class ChangeImageUrlToTextInServices < ActiveRecord::Migration
  def self.up
   change_column :services, :image_url, :text
  end

  def self.down
   # change_column :services, :image_url, :text
  end
end
