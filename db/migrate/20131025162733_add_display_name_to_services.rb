class AddDisplayNameToServices < ActiveRecord::Migration
  def change
    add_column :services, :display_name, :string
    add_column :services, :ecoupon_service_ids, :text
    add_column :services, :mytime_service_ids, :text
  end
end
