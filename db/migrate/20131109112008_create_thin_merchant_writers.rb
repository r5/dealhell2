class CreateThinMerchantWriters < ActiveRecord::Migration
  def up
    create_table :mthin_merchants do |t|
      t.string :id_hash
      t.string :yelp_id
      t.string :name
      t.decimal :lat, :precision => 15, :scale => 10
      t.decimal :lon, :precision => 15, :scale => 10 
      t.string :street_address
      t.string :city
      t.string :state
      t.integer :zip
      t.text :neighborhoods
      t.text :categories
      t.string :website
      t.string :phone_number
      t.float :rating
      t.integer :review_count
      t.float :yelp_rating
      t.integer :yelp_review_count
      t.string :yelp_rating_image_url
      t.text :hours
      t.string :parking
      t.integer :main_service_id
      t.text :service_id
      t.text :category_id
      t.string :source
      t.text :location
      t.string :price_range
      t.string :ypid
      t.text :location_description
      t.float :yp_rating
      t.integer :yp_review_count
      t.text :yp_hours
      t.text :yp_services
      t.text :merchant_association
      t.text :yp_general_info
      t.string :extra_phone
      t.string :payments
      t.text :accreditations
      t.text :amenities
      t.text :brands
      t.text :license
      t.integer :min_price
      t.integer :max_price
      t.boolean :already_indexed
      t.string :profile_url
      t.string :source_id
      t.string :redirect_url
      t.boolean :discrdable
      t.boolean :web
      t.text :about
      t.string :fbid
      t.integer :checkin_count
      t.integer :like_count
      t.boolean :mobile
      t.boolean :discardable
      t.boolean :blacklisted

      t.timestamps
    end
    execute 'alter table mthin_merchants add latlon point not null'
    execute 'alter table mthin_merchants engine myisam'
    # execute "update thin_merchants set lon = REPLACE(SUBSTRING_INDEX( location , '\nlon: ', -1 ), '\n', '')"
    # execute "update thin_merchants set lat = REPLACE(REPLACE(SUBSTRING_INDEX(location , '\nlon: ', 1 ), '\n', ''),'---lat: ', '')"
    execute 'update mthin_merchants set latlon = GeomFromText(concat(\'Point(\',lat,\' \',lon,\')\'))'
    execute "create trigger set_latlon_mthin_point before insert on mthin_merchants for each row set new.latlon = GeomFromText(concat('Point(',new.lat,' ',new.lon,')'));" 
    execute 'create spatial index thin_lat_lon on mthin_merchants(latlon)' 
    execute "CREATE FULLTEXT INDEX fulltext_thin_name ON mthin_merchants (name)"
    add_index :mthin_merchants, :main_service_id
  end

  def down
    drop_table :mthin_merchants
  end
end
