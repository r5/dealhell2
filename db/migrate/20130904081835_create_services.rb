class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.integer :category_id
      t.boolean :enabled
      t.integer :source_id
      t.integer :service_type
      t.string :source
      
      t.timestamps
    end
  end
end
