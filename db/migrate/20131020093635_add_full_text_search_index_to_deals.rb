class AddFullTextSearchIndexToDeals < ActiveRecord::Migration
  def up
    execute "CREATE FULLTEXT INDEX fulltext_name ON deals (name)"
  end

  def down
    execute "ALTER TABLE deals DROP INDEX fulltext_name"
  end
end
