class CreateZipCodes < ActiveRecord::Migration
  def change
    create_table :zip_codes do |t|
      t.string :city
      t.string :state
      t.string :zip
      t.string :area_code
      t.string :fips
      t.string :county
      t.string :time_zone
      t.boolean :dst
      t.string :lat
      t.string :lng
      t.string :utc_offset

      t.timestamps
    end
  end
end
