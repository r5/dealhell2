class AddCaptionToMyAttachment < ActiveRecord::Migration
  def change
    add_column :my_attachments, :caption, :string
  end
end
