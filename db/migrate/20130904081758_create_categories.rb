class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.boolean :enabled
      t.integer :source_id
      t.integer :cat_type
      t.string :source
      t.string :display_name
      t.string :status

      t.timestamps
    end
  end
end
