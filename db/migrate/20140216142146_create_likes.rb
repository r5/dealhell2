class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.integer :thin_merchant_id
      t.integer :user_id

      t.timestamps
    end
  end
end
