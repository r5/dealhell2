class AddCommentsCountToMyAttachments < ActiveRecord::Migration

  def self.up

    add_column :my_attachments, :comments_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :my_attachments, :comments_count

  end

end
