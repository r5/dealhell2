class AddUrlTypeToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :url_type, :string
  end
end
