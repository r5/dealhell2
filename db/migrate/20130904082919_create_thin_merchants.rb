class CreateThinMerchants < ActiveRecord::Migration
  def up
    create_table :thin_merchants do |t|
      t.string :id_hash
      t.string :yelp_id
      t.string :name
      t.decimal :lat, :precision => 15, :scale => 10
      t.decimal :lon, :precision => 15, :scale => 10 
      t.string :street_address
      t.string :city
      t.string :state
      t.integer :zip
      t.text :neighborhoods
      t.boolean :blacklisted
      t.text :categories
      t.string :website
      t.string :phone_number
      t.float :rating
      t.integer :review_count
      t.float :yelp_rating
      t.integer :yelp_review_count
      t.string :yelp_rating_image_url
      t.text :hours
      t.string :parking
      t.integer :main_service_id
      t.text :service_id
      t.text :category_id
      t.string :source
      t.text :location
      t.string :price_range
      t.string :ypid
      t.text :location_description
      t.float :yp_rating
      t.integer :yp_review_count
      t.text :yp_hours
      t.text :yp_services
      t.text :merchant_association
      t.text :yp_general_info
      t.string :extra_phone
      t.string :payments
      t.text :accreditations
      t.text :amenities
      t.text :brands
      t.text :license
      t.integer :min_price
      t.integer :max_price
      t.boolean :already_indexed
      t.string :profile_url
      t.string :source_id
      t.string :redirect_url
      t.boolean :discrdable
      t.boolean :web
      t.text :about
      t.string :fbid
      t.integer :checkin_count
      t.integer :like_count
      t.boolean :mobile

      t.timestamps
    end
  end

  def down  
    drop_table :thin_merchants
  end
end
