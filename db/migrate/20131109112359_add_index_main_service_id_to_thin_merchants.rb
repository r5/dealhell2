class AddIndexMainServiceIdToThinMerchants < ActiveRecord::Migration
  def change
    add_index :thin_merchants, :main_service_id
  end
end
