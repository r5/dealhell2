class AddSocialBookmarksCountToMyAttachments < ActiveRecord::Migration

  def self.up

    add_column :my_attachments, :social_bookmarks_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :my_attachments, :social_bookmarks_count

  end

end
