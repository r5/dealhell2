class CreateDealWriters < ActiveRecord::Migration
  def change
    create_table :mdeals do |t|
      t.boolean :affiliate
      t.string :name
      t.string :address
      t.string :address2
      t.integer :storeid
      t.integer :chainid
      t.integer :totaldealsinthisstore
      t.string :homepage
      t.integer :service_id
      t.string :phone
      t.string :state, :limit => 50
      t.string :city, :limit => 50
      t.integer :zip
      t.string :url
      t.string :storeurl
      t.string :dealsource
      t.string :user
      t.integer :userid
      t.integer :source_deal_id
      t.text :dealtitle
      t.text :disclaimer
      t.text :dealinfo
      t.date :expirationdate
      t.datetime :postdate
      t.string :showimage
      t.string :showimagestandardbig
      t.string :showimagestandardsmall
      t.string :showlogo
      t.integer :up
      t.integer :down
      t.integer :dealtypeid
      t.integer :categoryid
      t.integer :subcategoryid
      t.decimal :lat, :precision => 15, :scale => 10
      t.decimal :lon, :precision => 15, :scale => 10  
      t.float :distance
      t.float :dealoriginalprice
      t.float :dealprice
      t.float :dealsavings
      t.float :dealdiscountpercent
      t.string :affiliate_source
      t.string :my_deal_type
      t.integer :sub_category_id
      t.integer :category_id
      t.text :modifiers
      t.text :next_appointment_times
      t.text :neighborhoods
      t.integer :url_type
      t.timestamps
    end
    add_index :mdeals, :service_id
    execute 'alter table mdeals add latlon point not null'
    execute 'alter table mdeals engine myisam'
    execute 'update mdeals set latlon = GeomFromText(concat(\'Point(\',lat,\' \',lon,\')\'))' 
    execute 'create spatial index mdeals_lat_lon on mdeals(latlon)' 
    execute "CREATE FULLTEXT INDEX fulltext_name ON mdeals (name)"
    execute "ALTER TABLE mdeals charset=utf8mb4, MODIFY COLUMN name VARCHAR(255) CHARACTER SET utf8mb4,MODIFY COLUMN homepage VARCHAR(255) CHARACTER SET utf8mb4,MODIFY COLUMN address VARCHAR(255) CHARACTER SET utf8mb4,MODIFY COLUMN address2 text CHARACTER SET utf8mb4,MODIFY COLUMN storeurl VARCHAR(255) CHARACTER SET utf8mb4, MODIFY COLUMN user VARCHAR(255) CHARACTER SET utf8mb4,MODIFY COLUMN dealtitle VARCHAR(255) CHARACTER SET utf8mb4, MODIFY COLUMN disclaimer text CHARACTER SET utf8mb4,MODIFY COLUMN dealinfo text CHARACTER SET utf8mb4, MODIFY COLUMN showimage VARCHAR(255) CHARACTER SET utf8mb4, MODIFY COLUMN showimagestandardbig VARCHAR(255) CHARACTER SET utf8mb4, MODIFY COLUMN showimagestandardsmall VARCHAR(255) CHARACTER SET utf8mb4, MODIFY COLUMN showlogo VARCHAR(255) CHARACTER SET utf8mb4, MODIFY COLUMN affiliate_source VARCHAR(255) CHARACTER SET utf8mb4, MODIFY COLUMN my_deal_type VARCHAR(255) CHARACTER SET utf8mb4;"
    # execute "create trigger set_latlon_on_deals before insert on deals for each row set new.latlon = GeomFromText(concat('Point(',new.lat,' ',new.lon,')'));" 
    # References: http://stackoverflow.com/questions/5756232/moving-lat-lon-text-columns-into-a-point-type-column
    # References: http://www.elevatedcode.com/2009/03/06/speeding-up-location-based-searches.html
    # ActiveRecord::Base.connection.execute('alter table deals add latlon point not null')
    # ActiveRecord::Base.connection.execute('alter table deals engine myisam')
    #ActiveRecord::Base.connection.execute("UPDATE deals SET latlon = Point(lat, lon);")
    #ActiveRecord::Base.connection.execute("CREATE SPATIAL INDEX deals_lat_lon ON deals(latlon);")
  end
end
