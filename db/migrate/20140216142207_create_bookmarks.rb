class CreateBookmarks < ActiveRecord::Migration
  def change
    ActiveRecord::Migration.create_table :bookmarks do |t|
      t.integer :thin_merchant_id
      t.integer :user_id

      t.timestamps
    end
  end
end
