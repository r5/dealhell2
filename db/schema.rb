# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131016122727) do

  create_table "categories", force: true do |t|
    t.string   "name"
    t.boolean  "enabled"
    t.integer  "source_id"
    t.integer  "cat_type"
    t.string   "source"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deals", force: true do |t|
    t.boolean  "affiliate"
    t.string   "name"
    t.string   "address"
    t.string   "address2"
    t.integer  "storeid"
    t.integer  "chainid"
    t.integer  "totaldealsinthisstore"
    t.string   "homepage"
    t.string   "phone"
    t.string   "state",                  limit: 50
    t.string   "city",                   limit: 50
    t.integer  "zip"
    t.string   "url"
    t.string   "storeurl"
    t.string   "dealsource"
    t.string   "user"
    t.integer  "userid"
    t.integer  "source_deal_id"
    t.text     "dealtitle"
    t.text     "disclaimer"
    t.text     "dealinfo"
    t.date     "expirationdate"
    t.datetime "postdate"
    t.string   "showimage"
    t.string   "showimagestandardbig"
    t.string   "showimagestandardsmall"
    t.string   "showlogo"
    t.integer  "up"
    t.integer  "down"
    t.integer  "dealtypeid"
    t.integer  "categoryid"
    t.integer  "subcategoryid"
    t.float    "lat"
    t.float    "lon"
    t.float    "distance"
    t.float    "dealoriginalprice"
    t.float    "dealprice"
    t.float    "dealsavings"
    t.float    "dealdiscountpercent"
    t.string   "affiliate_source"
    t.integer  "sub_category_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "my_deal_type"
  end

  create_table "ga_city_counties", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "services", force: true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.boolean  "enabled"
    t.integer  "source_id"
    t.integer  "service_type"
    t.string   "source"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sub_categories", force: true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.boolean  "enabled"
    t.integer  "source_id"
    t.integer  "service_type"
    t.string   "source"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "thin_merchants", force: true do |t|
    t.string   "id_hash"
    t.integer  "latlon",                limit: nil, null: false
    t.string   "yelp_id"
    t.string   "name"
    t.string   "street_address"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.text     "neighborhoods"
    t.text     "categories"
    t.string   "website"
    t.string   "phone_number"
    t.float    "rating"
    t.integer  "review_count"
    t.float    "yelp_rating"
    t.integer  "yelp_review_count"
    t.string   "yelp_rating_image_url"
    t.text     "hours"
    t.string   "parking"
    t.integer  "main_service_id"
    t.text     "service_id"
    t.text     "category_id"
    t.string   "source"
    t.text     "location"
    t.string   "price_range"
    t.string   "ypid"
    t.text     "location_description"
    t.float    "yp_rating"
    t.integer  "yp_review_count"
    t.text     "yp_hours"
    t.text     "yp_services"
    t.text     "merchant_association"
    t.text     "yp_general_info"
    t.string   "extra_phone"
    t.string   "payments"
    t.text     "accreditations"
    t.text     "amenities"
    t.text     "brands"
    t.text     "license"
    t.integer  "min_price"
    t.integer  "max_price"
    t.boolean  "already_indexed"
    t.string   "profile_url"
    t.string   "source_id"
    t.string   "redirect_url"
    t.boolean  "discrdable"
    t.boolean  "web"
    t.text     "about"
    t.string   "fbid"
    t.integer  "checkin_count"
    t.integer  "like_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "zip_codes", force: true do |t|
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "area_code"
    t.string   "fips"
    t.string   "county"
    t.string   "time_zone"
    t.boolean  "dst"
    t.string   "lat"
    t.string   "lng"
    t.string   "utc_offset"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
