-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: trenato_development_new
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `pet_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookmarks`
--

DROP TABLE IF EXISTS `bookmarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thin_merchant_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_bookmarks_on_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `average_discount` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suggested_at` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postion` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_categories_on_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `my_attachment_id` int(11) DEFAULT NULL,
  `comment_text` text COLLATE utf8_unicode_ci,
  `spam` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_comments_on_user_id` (`user_id`),
  KEY `index_comments_on_my_attachment_id` (`my_attachment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deals`
--

DROP TABLE IF EXISTS `deals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate` tinyint(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storeid` int(11) DEFAULT NULL,
  `chainid` int(11) DEFAULT NULL,
  `totaldealsinthisstore` int(11) DEFAULT NULL,
  `homepage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storeurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealsource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `source_deal_id` int(11) DEFAULT NULL,
  `dealtitle` text COLLATE utf8_unicode_ci,
  `disclaimer` text COLLATE utf8_unicode_ci,
  `dealinfo` text COLLATE utf8_unicode_ci,
  `expirationdate` date DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `showimage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showimagestandardbig` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showimagestandardsmall` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showlogo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `up` int(11) DEFAULT NULL,
  `down` int(11) DEFAULT NULL,
  `dealtypeid` int(11) DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `subcategoryid` int(11) DEFAULT NULL,
  `lat` decimal(15,10) DEFAULT NULL,
  `lon` decimal(15,10) DEFAULT NULL,
  `distance` float DEFAULT NULL,
  `dealoriginalprice` float DEFAULT NULL,
  `dealprice` float DEFAULT NULL,
  `dealsavings` float DEFAULT NULL,
  `dealdiscountpercent` float DEFAULT NULL,
  `affiliate_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `my_deal_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `modifiers` text COLLATE utf8_unicode_ci,
  `next_appointment_times` text COLLATE utf8_unicode_ci,
  `neighborhoods` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `latlon` point NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `url_type` int(11) DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avg_rating` float DEFAULT NULL,
  `review_count` int(11) DEFAULT NULL,
  `company_detail` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  SPATIAL KEY `deals_lat_lon` (`latlon`),
  KEY `index_deals_on_service_id` (`service_id`),
  KEY `index_deals_on_expirationdate` (`expirationdate`),
  FULLTEXT KEY `fulltext_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=52455 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ga_city_counties`
--

DROP TABLE IF EXISTS `ga_city_counties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ga_city_counties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thin_merchant_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `my_attachment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_likes_on_user_id` (`user_id`),
  KEY `index_likes_on_my_attachment_id` (`my_attachment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mdeals`
--

DROP TABLE IF EXISTS `mdeals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdeals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate` tinyint(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storeid` int(11) DEFAULT NULL,
  `chainid` int(11) DEFAULT NULL,
  `totaldealsinthisstore` int(11) DEFAULT NULL,
  `homepage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storeurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealsource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `source_deal_id` int(11) DEFAULT NULL,
  `dealtitle` text COLLATE utf8_unicode_ci,
  `disclaimer` text COLLATE utf8_unicode_ci,
  `dealinfo` text COLLATE utf8_unicode_ci,
  `expirationdate` date DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `showimage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showimagestandardbig` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showimagestandardsmall` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showlogo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `up` int(11) DEFAULT NULL,
  `down` int(11) DEFAULT NULL,
  `dealtypeid` int(11) DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `subcategoryid` int(11) DEFAULT NULL,
  `lat` decimal(15,10) DEFAULT NULL,
  `lon` decimal(15,10) DEFAULT NULL,
  `distance` float DEFAULT NULL,
  `dealoriginalprice` float DEFAULT NULL,
  `dealprice` float DEFAULT NULL,
  `dealsavings` float DEFAULT NULL,
  `dealdiscountpercent` float DEFAULT NULL,
  `affiliate_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `my_deal_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `modifiers` text COLLATE utf8_unicode_ci,
  `next_appointment_times` text COLLATE utf8_unicode_ci,
  `neighborhoods` text COLLATE utf8_unicode_ci,
  `url_type` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `latlon` point NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avg_rating` float DEFAULT NULL,
  `review_count` int(11) DEFAULT NULL,
  `company_detail` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `index_mdeals_on_service_id` (`service_id`),
  SPATIAL KEY `mdeals_lat_lon` (`latlon`),
  KEY `index_mdeals_on_expirationdate` (`expirationdate`),
  FULLTEXT KEY `fulltext_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=1478 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mthin_merchants`
--

DROP TABLE IF EXISTS `mthin_merchants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mthin_merchants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yelp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` decimal(15,10) DEFAULT NULL,
  `lon` decimal(15,10) DEFAULT NULL,
  `street_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `neighborhoods` text COLLATE utf8_unicode_ci,
  `categories` text COLLATE utf8_unicode_ci,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `review_count` int(11) DEFAULT NULL,
  `yelp_rating` float DEFAULT NULL,
  `yelp_review_count` int(11) DEFAULT NULL,
  `yelp_rating_image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hours` text COLLATE utf8_unicode_ci,
  `parking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_service_id` int(11) DEFAULT NULL,
  `service_id` text COLLATE utf8_unicode_ci,
  `category_id` text COLLATE utf8_unicode_ci,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8_unicode_ci,
  `price_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ypid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_description` text COLLATE utf8_unicode_ci,
  `yp_rating` float DEFAULT NULL,
  `yp_review_count` int(11) DEFAULT NULL,
  `yp_hours` text COLLATE utf8_unicode_ci,
  `yp_services` text COLLATE utf8_unicode_ci,
  `merchant_association` text COLLATE utf8_unicode_ci,
  `yp_general_info` text COLLATE utf8_unicode_ci,
  `extra_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accreditations` text COLLATE utf8_unicode_ci,
  `amenities` text COLLATE utf8_unicode_ci,
  `brands` text COLLATE utf8_unicode_ci,
  `license` text COLLATE utf8_unicode_ci,
  `min_price` int(11) DEFAULT NULL,
  `max_price` int(11) DEFAULT NULL,
  `already_indexed` tinyint(1) DEFAULT NULL,
  `profile_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discrdable` tinyint(1) DEFAULT NULL,
  `web` tinyint(1) DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci,
  `fbid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkin_count` int(11) DEFAULT NULL,
  `like_count` int(11) DEFAULT NULL,
  `mobile` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `latlon` point NOT NULL,
  PRIMARY KEY (`id`),
  SPATIAL KEY `thin_lat_lon` (`latlon`),
  KEY `index_mthin_merchants_on_main_service_id` (`main_service_id`),
  FULLTEXT KEY `fulltext_thin_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger set_latlon_mthin_point before insert on mthin_merchants for each row set new.latlon = GeomFromText(concat('Point(',new.lat,' ',new.lon,')')) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `my_attachments`
--

DROP TABLE IF EXISTS `my_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `pet_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `asset_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_file_size` int(11) DEFAULT NULL,
  `asset_updated_at` datetime DEFAULT NULL,
  `reposts_count` int(11) NOT NULL DEFAULT '0',
  `likes_count` int(11) NOT NULL DEFAULT '0',
  `social_bookmarks_count` int(11) NOT NULL DEFAULT '0',
  `comments_count` int(11) NOT NULL DEFAULT '0',
  `album_id` int(11) DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1050 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read` tinyint(1) DEFAULT NULL,
  `slug_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pet_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `action_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_notifications_on_user_id` (`user_id`),
  KEY `index_notifications_on_actor_id` (`actor_id`),
  KEY `index_notifications_on_pet_id` (`pet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pets`
--

DROP TABLE IF EXISTS `pets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `breed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `likes_count` int(11) DEFAULT NULL,
  `reposts_count` int(11) DEFAULT NULL,
  `about_pet` text COLLATE utf8_unicode_ci,
  `owner_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate` tinyint(1) DEFAULT NULL,
  `dealsource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_source_id` int(11) DEFAULT NULL,
  `dealtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showimage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expirationdate` date DEFAULT NULL,
  `postdata` datetime DEFAULT NULL,
  `storeurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chainid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recommendations`
--

DROP TABLE IF EXISTS `recommendations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommendations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `thin_merchant_id` int(11) DEFAULT NULL,
  `rtext` text COLLATE utf8_unicode_ci,
  `published` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_recommendations_on_user_id` (`user_id`),
  KEY `index_recommendations_on_thin_merchant_id` (`thin_merchant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relationships`
--

DROP TABLE IF EXISTS `relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `follower_id` int(11) DEFAULT NULL,
  `followed_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_relationships_on_follower_id_and_followed_id` (`follower_id`,`followed_id`),
  KEY `index_relationships_on_follower_id` (`follower_id`),
  KEY `index_relationships_on_followed_id` (`followed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reposts`
--

DROP TABLE IF EXISTS `reposts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reposts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `my_attachment_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `label` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `index_reposts_on_user_id` (`user_id`),
  KEY `index_reposts_on_my_attachment_id` (`my_attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `saves`
--

DROP TABLE IF EXISTS `saves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thin_merchant_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suggested_at` datetime DEFAULT NULL,
  `average_discount` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecoupon_service_ids` text COLLATE utf8_unicode_ci,
  `mytime_service_ids` text COLLATE utf8_unicode_ci,
  `eight_coupons_subcat_id` int(11) DEFAULT NULL,
  `image_url` text COLLATE utf8_unicode_ci,
  `ass_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_services_on_category_id` (`category_id`),
  KEY `index_services_on_status` (`status`),
  KEY `index_services_on_ass_id` (`ass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_bookmarks`
--

DROP TABLE IF EXISTS `social_bookmarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_bookmarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `my_attachment_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_social_bookmarks_on_user_id` (`user_id`),
  KEY `index_social_bookmarks_on_my_attachment_id` (`my_attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `thin_merchants`
--

DROP TABLE IF EXISTS `thin_merchants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thin_merchants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yelp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `neighborhoods` mediumtext COLLATE utf8_unicode_ci,
  `categories` mediumtext COLLATE utf8_unicode_ci,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `review_count` int(11) DEFAULT NULL,
  `yelp_rating` float DEFAULT NULL,
  `yelp_review_count` int(11) DEFAULT NULL,
  `yelp_rating_image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hours` mediumtext COLLATE utf8_unicode_ci,
  `parking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_service_id` int(11) DEFAULT NULL,
  `service_id` mediumtext COLLATE utf8_unicode_ci,
  `category_id` mediumtext COLLATE utf8_unicode_ci,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` mediumtext COLLATE utf8_unicode_ci,
  `price_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ypid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_description` mediumtext COLLATE utf8_unicode_ci,
  `yp_rating` float DEFAULT NULL,
  `yp_review_count` int(11) DEFAULT NULL,
  `yp_hours` mediumtext COLLATE utf8_unicode_ci,
  `yp_services` mediumtext COLLATE utf8_unicode_ci,
  `merchant_association` mediumtext COLLATE utf8_unicode_ci,
  `yp_general_info` mediumtext COLLATE utf8_unicode_ci,
  `extra_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accreditations` mediumtext COLLATE utf8_unicode_ci,
  `amenities` mediumtext COLLATE utf8_unicode_ci,
  `brands` mediumtext COLLATE utf8_unicode_ci,
  `license` mediumtext COLLATE utf8_unicode_ci,
  `already_indexed` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `min_price` int(11) DEFAULT NULL,
  `max_price` int(11) DEFAULT NULL,
  `profile_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` tinyint(1) DEFAULT '0',
  `web` tinyint(1) DEFAULT '0',
  `discardable` tinyint(1) DEFAULT '0',
  `about` mediumtext COLLATE utf8_unicode_ci,
  `fbid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkin_count` int(11) DEFAULT NULL,
  `like_count` int(11) DEFAULT NULL,
  `redirect_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blacklisted` tinyint(1) DEFAULT '0',
  `blacklisted_by_company_id` int(11) DEFAULT NULL,
  `visits_count` int(11) DEFAULT NULL,
  `matched_thin_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yelp_reviews` mediumtext COLLATE utf8_unicode_ci,
  `google_reviews` mediumtext COLLATE utf8_unicode_ci,
  `city_reviews` mediumtext COLLATE utf8_unicode_ci,
  `yelp_reviews_updated_at` datetime DEFAULT NULL,
  `city_reviews_updated_at` datetime DEFAULT NULL,
  `google_reviews_updated_at` datetime DEFAULT NULL,
  `sources` mediumtext COLLATE utf8_unicode_ci,
  `locu_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` decimal(15,10) DEFAULT NULL,
  `lon` decimal(15,10) DEFAULT NULL,
  `latlon` point NOT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `bookmarks_count` int(11) NOT NULL DEFAULT '0',
  `recommendations_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  SPATIAL KEY `thin_lat_lon` (`latlon`),
  FULLTEXT KEY `fulltext_thin_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3277931 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file_size` int(11) DEFAULT NULL,
  `avatar_updated_at` datetime DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_toke` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zip_codes`
--

DROP TABLE IF EXISTS `zip_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zip_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_code` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fips` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_zone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dst` tinyint(1) DEFAULT NULL,
  `lat` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_type` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `utc_offset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foursquare_scraped` tinyint(1) DEFAULT NULL,
  `facebook_scraped` tinyint(1) DEFAULT NULL,
  `yelp_searched` tinyint(1) DEFAULT NULL,
  `latlon` point NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_zip_codes_on_zip` (`zip`),
  SPATIAL KEY `thin_lat_lon` (`latlon`),
  FULLTEXT KEY `fulltext_location` (`city`,`state`,`zip`,`county`)
) ENGINE=MyISAM AUTO_INCREMENT=41887 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-12 21:32:01
INSERT INTO schema_migrations (version) VALUES ('20130904081225');

INSERT INTO schema_migrations (version) VALUES ('20130904081758');

INSERT INTO schema_migrations (version) VALUES ('20130904081835');

INSERT INTO schema_migrations (version) VALUES ('20130904082919');

INSERT INTO schema_migrations (version) VALUES ('20130915040740');

INSERT INTO schema_migrations (version) VALUES ('20130915091024');

INSERT INTO schema_migrations (version) VALUES ('20130915092736');

INSERT INTO schema_migrations (version) VALUES ('20131016122727');

INSERT INTO schema_migrations (version) VALUES ('20131020093635');

INSERT INTO schema_migrations (version) VALUES ('20131020150309');

INSERT INTO schema_migrations (version) VALUES ('20131020155950');

INSERT INTO schema_migrations (version) VALUES ('20131025162733');

INSERT INTO schema_migrations (version) VALUES ('20131025162814');

INSERT INTO schema_migrations (version) VALUES ('20131025181951');

INSERT INTO schema_migrations (version) VALUES ('20131025184531');

INSERT INTO schema_migrations (version) VALUES ('20131025190117');

INSERT INTO schema_migrations (version) VALUES ('20131025192015');

INSERT INTO schema_migrations (version) VALUES ('20131101162218');

INSERT INTO schema_migrations (version) VALUES ('20131104111305');

INSERT INTO schema_migrations (version) VALUES ('20131109062343');

INSERT INTO schema_migrations (version) VALUES ('20131109111618');

INSERT INTO schema_migrations (version) VALUES ('20131109112008');

INSERT INTO schema_migrations (version) VALUES ('20131109112359');

INSERT INTO schema_migrations (version) VALUES ('20131111145812');

INSERT INTO schema_migrations (version) VALUES ('20131114144344');

INSERT INTO schema_migrations (version) VALUES ('20131114144635');

INSERT INTO schema_migrations (version) VALUES ('20131114152321');

INSERT INTO schema_migrations (version) VALUES ('20131116100340');

INSERT INTO schema_migrations (version) VALUES ('20131116112806');

INSERT INTO schema_migrations (version) VALUES ('20131116123245');

INSERT INTO schema_migrations (version) VALUES ('20140209051933');

INSERT INTO schema_migrations (version) VALUES ('20140209105337');

INSERT INTO schema_migrations (version) VALUES ('20140214170903');

INSERT INTO schema_migrations (version) VALUES ('20140215122346');

INSERT INTO schema_migrations (version) VALUES ('20140216142146');

INSERT INTO schema_migrations (version) VALUES ('20140216142207');

INSERT INTO schema_migrations (version) VALUES ('20140216143452');

INSERT INTO schema_migrations (version) VALUES ('20140216143522');

INSERT INTO schema_migrations (version) VALUES ('20140220155450');

INSERT INTO schema_migrations (version) VALUES ('20140220155643');

INSERT INTO schema_migrations (version) VALUES ('20140220155657');

INSERT INTO schema_migrations (version) VALUES ('20140221142940');

INSERT INTO schema_migrations (version) VALUES ('20140222112655');

INSERT INTO schema_migrations (version) VALUES ('20140223113608');

INSERT INTO schema_migrations (version) VALUES ('20140225094721');

INSERT INTO schema_migrations (version) VALUES ('20140225095030');

INSERT INTO schema_migrations (version) VALUES ('20140227132511');

INSERT INTO schema_migrations (version) VALUES ('20140227132929');

INSERT INTO schema_migrations (version) VALUES ('20140227133355');

INSERT INTO schema_migrations (version) VALUES ('20140227133403');

INSERT INTO schema_migrations (version) VALUES ('20140227135432');

INSERT INTO schema_migrations (version) VALUES ('20140227135837');

INSERT INTO schema_migrations (version) VALUES ('20140227143029');

INSERT INTO schema_migrations (version) VALUES ('20140227143157');

INSERT INTO schema_migrations (version) VALUES ('20140227143644');

INSERT INTO schema_migrations (version) VALUES ('20140227144638');

INSERT INTO schema_migrations (version) VALUES ('20140227150848');

INSERT INTO schema_migrations (version) VALUES ('20140308071155');

INSERT INTO schema_migrations (version) VALUES ('20140308071320');

INSERT INTO schema_migrations (version) VALUES ('20140308084421');

INSERT INTO schema_migrations (version) VALUES ('20140312093746');

INSERT INTO schema_migrations (version) VALUES ('20140312105230');

INSERT INTO schema_migrations (version) VALUES ('20140312160142');
